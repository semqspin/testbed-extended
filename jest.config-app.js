const _ = require('lodash');
const baseConfig = require('./jest.config-base');

module.exports = _.merge(baseConfig, {
    globals: {
        'ts-jest': {
            allowJs: true,
        },
    },
    roots: ['src'],
    moduleNameMapper: {
        '@smazzoleni/(.*)': '<rootDir>/dist/$1',
    },
});

module.exports.transform = { '^.+\\.(ts|html)$': 'ts-jest' };
