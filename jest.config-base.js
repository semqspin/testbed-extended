const _ = require('lodash');
const jestPreset = require('jest-preset-angular/jest-preset');

module.exports = _.merge(jestPreset, {
    cacheDirectory: '<rootDir>/tmp/jest',
    setupFilesAfterEnv: [
        '@angular-builders/jest/dist/jest-config/setup.js',
        'jest-extended',
        '@testing-library/jest-dom',
        '<rootDir>/jest.setup.ts',
    ],
    globals: {
        'ts-jest': {
            tsconfig: '<rootDir>/tsconfig.spec.json',
        },
    },
});
