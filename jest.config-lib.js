const _ = require('lodash');
const baseConfig = require('./jest.config-base');

module.exports = _.merge(baseConfig, {
    roots: ['projects'],
});
