module.exports = {
    arrowParens: 'avoid',
    bracketSpacing: true,
    printWidth: 80,
    quoteProps: 'as-needed',
    semi: true,
    singleQuote: true,
    tabWidth: 4,
    trailingComma: 'all',
    useTabs: false,
    overrides: [
        {
            files: ['*.html', '*.json'],
            options: {
                tabWidth: 2,
            },
        },
    ],
};