import { Subject } from 'rxjs';
import { DetectChangesPatcher } from './detect-changes-patcher';
import { Fake } from './fake-type';
import { Host } from './host';

describe('DetectChangesPatcher', () => {
    describe('patchMethodInvocation', () => {
        it('once patched, a method should trigger automatically host.detectChanges when invoked', () => {
            const subject = new Subject<string>();
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            patcher.patchMethodInvocation(subject, 'next');

            subject.next();
            expect(fakeHost.detectChanges).toBeCalled();
        });

        it('host.detectChanges should be called AFTER the patched method is invoked', () => {
            const subject = new Subject<string>();
            const spy = jest.spyOn(subject, 'next');
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            patcher.patchMethodInvocation(subject, 'next');

            subject.next();
            expect(fakeHost.detectChanges).toHaveBeenCalledAfter(spy as any);
        });

        it('patched method can be invoked before host is set on the pather', () => {
            const subject = new Subject<string>();
            const patcher = new DetectChangesPatcher();

            patcher.patchMethodInvocation(subject, 'next');

            subject.next();
        });

        it('when patched several times, a method should trigger automatically host.detectChanges only once when invoked', () => {
            const subject = new Subject<string>();
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            patcher.patchMethodInvocation(subject, 'next');
            patcher.patchMethodInvocation(subject, 'next');

            subject.next();
            expect(fakeHost.detectChanges).toBeCalledTimes(1);
        });
    });

    describe('patchSetters', () => {
        it('once patch, should trigger detectChanges each time a property is set on the patched return object', () => {
            const person: { firstName?: string } = {};
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            const patchedPerson = patcher.patchPropertySetters(person);

            patchedPerson.firstName = 'Sergio';
            expect(fakeHost.detectChanges).toBeCalled();
        });

        it('[DO NOT] setting a property on initial object will NOT trigger detectChanges', () => {
            const person: { firstName?: string } = {};
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            patcher.patchPropertySetters(person);

            person.firstName = 'Sergio';
            expect(fakeHost.detectChanges).not.toBeCalled();
        });

        it('should patch "next" of the property value if value is a subject', () => {
            const initialObject: { message$?: Subject<string> } = {};
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            const patchedObject = patcher.patchPropertySetters(initialObject);

            patchedObject.message$ = new Subject();
            fakeHost.detectChanges.mockClear();

            patchedObject.message$.next('one');
            patchedObject.message$.next('two');
            expect(fakeHost.detectChanges).toBeCalledTimes(2);
        });

        it('if property value is a function, and function call returns a subject, should patch "next" invocation of that returned subject', () => {
            const initialObject: {
                getMessage$?: jest.Mock<Subject<string>>;
            } = {};
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            const patchedObject = patcher.patchPropertySetters(initialObject);

            patchedObject.getMessage$ = jest
                .fn()
                .mockReturnValue(new Subject<string>());
            fakeHost.detectChanges.mockClear();

            const messageSubject = patchedObject.getMessage$();

            messageSubject.next('one');
            messageSubject.next('two');
            expect(fakeHost.detectChanges).toBeCalledTimes(2);
        });

        it('should patch returning subjects that already exists on the object when it is patched', () => {
            const initialObject = {
                getMessage$: jest.fn().mockReturnValue(new Subject<string>()),
            };
            const fakeHost = { detectChanges: jest.fn() } as Fake<Host<any>>;
            const patcher = new DetectChangesPatcher();
            patcher.host = fakeHost as any;

            const patchedObject = patcher.patchPropertySetters(initialObject);

            const messageSubject = patchedObject.getMessage$();
            fakeHost.detectChanges.mockClear();

            messageSubject.next('one');
            messageSubject.next('two');
            expect(fakeHost.detectChanges).toBeCalledTimes(2);
        });

        it('should not patch an already patched object and return it as is', () => {
            const initialObject = {};

            const patcher = new DetectChangesPatcher();

            const patchedObject = patcher.patchPropertySetters(initialObject);

            expect(patcher.patchPropertySetters(patchedObject)).toBe(
                patchedObject,
            );
        });

        it('should return same proxy when patching several times the same object', () => {
            const initialObject = {};

            const patcher = new DetectChangesPatcher();

            const patchedObject1 = patcher.patchPropertySetters(initialObject);
            const patchedObject2 = patcher.patchPropertySetters(initialObject);

            expect(patchedObject1).toBe(patchedObject2);
        });
    });
});
