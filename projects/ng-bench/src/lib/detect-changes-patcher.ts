import _ from 'lodash';
import { Subject } from 'rxjs';
import { Host } from './host';

const PATCHED_METHODS = Symbol('PATCHED_METHODS');
const IS_PATCHED_BY = Symbol('IS_PATCHED_BY');
const IS_PROXY_FOR = Symbol('IS_PROXY_FOR');

export class DetectChangesPatcher {
    public host: Host<any> | null = null;

    patchMethodInvocation<T>(obj: T, key: keyof T) {
        if (this.isNotAlreadyMarkedAsPatched(obj, key)) {
            this.doPatchMethodInvocation(obj, key);
            this.markAsPatched(obj, key);
        }
    }

    private isNotAlreadyMarkedAsPatched<T>(obj: T, key: keyof T): boolean {
        return !_.get(obj, [PATCHED_METHODS, key]);
    }

    private doPatchMethodInvocation<T>(obj: T, key: keyof T): void {
        obj[key] = new Proxy(obj[key] as any, {
            apply: this.detectChangesAfterApply.bind(this),
        });
    }

    private detectChangesAfterApply(
        target: any,
        thisArg: any,
        thisArgArray: any,
    ) {
        target.apply(thisArg, thisArgArray);
        this.detectChanges();
    }

    private markAsPatched<T>(obj: T, key: keyof T): void {
        _.set(obj as any, [PATCHED_METHODS, key], true);
    }

    private detectChanges() {
        this.host?.detectChanges();
    }

    patchPropertySetters<T>(objectToPatch: T): T {
        if (_.get(objectToPatch, IS_PROXY_FOR)) {
            return objectToPatch;
        }

        if (!_.get(objectToPatch, IS_PATCHED_BY)) {
            const proxy = new Proxy(
                { [IS_PROXY_FOR]: objectToPatch },
                { set: this.detectChangesAfterSet.bind(this) },
            );
            Object.assign(proxy, objectToPatch);
            _.set(objectToPatch as any, IS_PATCHED_BY, proxy);
        }
        return _.get(objectToPatch, IS_PATCHED_BY);
    }

    private detectChangesAfterSet(obj: any, prop: string, value: any) {
        obj[prop] = value;
        if (value instanceof Subject) {
            this.patchMethodInvocation(value, 'next');
        } else if (_.isFunction(value)) {
            obj[prop] = this.patchFunctionReturningSubject(value);
        }
        this.detectChanges();
        return true;
    }

    private patchFunctionReturningSubject(value: (...args: any[]) => any): any {
        return new Proxy(value, {
            apply: this.patchReturnedSubject.bind(this),
        });
    }

    private patchReturnedSubject(target: any, thisArg: any, thisArgArray: any) {
        const res = target.apply(thisArg, thisArgArray);
        if (res instanceof Subject) {
            this.patchMethodInvocation(res, 'next');
        }
        return res;
    }
}
