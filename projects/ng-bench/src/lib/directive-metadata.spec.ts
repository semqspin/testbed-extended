import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    Output,
    ViewChild,
} from '@angular/core';
import { DirectiveMetadata } from './directive-metadata';

@Component({
    selector: 'sma-awesome',
})
class SomeComponent {
    // tslint:disable: no-input-rename
    @Input('first') firstName = '';
    @Input('last') lastName = '';
    @Input() birthdate = new Date();
    @Input() set age(value: number) {
        this.somePrivateProperty = value;
    }

    @Output() delete = new EventEmitter();
    @Output() save = new EventEmitter();

    // @ts-ignore
    private somePrivateProperty: any;
    public somePublicProperty: any;
    @ViewChild('whatever', { static: true })
    someViewChildAnnotatedProperty: ElementRef = null as any;

    method1() {}
    method2() {}
}

describe('DirectiveMetadata', () => {
    let metadata: DirectiveMetadata<SomeComponent>;

    beforeEach(() => {
        metadata = new DirectiveMetadata(SomeComponent);
    });

    it('should get the selector', () => {
        expect(metadata.getSelector()).toBe('sma-awesome');
    });

    // TODO: since Angular 13 / TypeScript 4.5, metadata changed.
    // find a way to retrieve this
    it.skip('should get the aliases that can be used in templates', () => {
        expect(metadata.getBindingPropertyAliases()).toEqual({
            firstName: 'first',
            lastName: 'last',
            birthdate: 'birthdate',
            age: 'age',
            delete: 'delete',
            save: 'save',
        });
    });

    // TODO: since Angular 13 / TypeScript 4.5, metadata changed.
    // find a way to retrieve this
    it.skip('getInvalidBindingProperties should return propertyNames ', () => {
        expect(
            metadata.getInvalidBindingProperties(
                'firstName',
                'lastName',
                'age',
                'method1',
                'someViewChildAnnotatedProperty',
                'somePublicProperty',
            ),
        ).toIncludeSameMembers([
            'method1',
            'someViewChildAnnotatedProperty',
            'somePublicProperty',
        ]);
    });

    describe('getNonDecoratedProperties', () => {
        it('should return properties that are not decorated', () => {
            expect(
                metadata.getNonDecoratedProperties(
                    'firstName',
                    'lastName',
                    'age',
                    'method1',
                    'someViewChildAnnotatedProperty',
                    'somePublicProperty',
                ),
            ).toIncludeSameMembers(['method1', 'somePublicProperty']);
        });
    });

    describe('getDecoratedPropertyAliases', () => {
        it('should return a map (object) where key is property name and value first decorator argument (defaut is property name)', () => {
            expect(metadata.getDecoratedPropertyAliases()).toEqual({
                firstName: 'first',
                lastName: 'last',
                birthdate: 'birthdate',
                age: 'age',
                delete: 'delete',
                save: 'save',
                someViewChildAnnotatedProperty: 'whatever',
            });
        });
    });
});
