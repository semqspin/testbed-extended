import { Type } from '@angular/core';
import _ from 'lodash';

export class DirectiveMetadata<T> {
    // tslint:disable-next-line: variable-name
    constructor(private _type: Type<T>) {}

    get type() {
        return this._type;
    }

    get directiveName() {
        return this._type.name;
    }

    getSelector(): string {
        return (this._type as any).__annotations__[0].selector;
    }

    getBindingPropertyAliases() {
        const metadata = Reflect.get(this._type, '__prop__metadata__');

        return _.chain(this.getBindingProperties())
            .map(propertyName => [
                propertyName,
                metadata[propertyName][0].bindingPropertyName || propertyName,
            ])
            .fromPairs()
            .toPlainObject()
            .value();
    }

    getInvalidBindingProperties(...keys: (keyof T)[]): (keyof T)[] {
        return _.difference(keys, this.getBindingProperties());
    }

    private getBindingProperties(): (keyof T)[] {
        const metadata = Reflect.get(this._type, '__prop__metadata__');
        return _.chain(metadata)
            .keys()
            .filter(key => 'bindingPropertyName' in metadata[key][0])
            .map(key => key as keyof T)
            .value();
    }

    getNonDecoratedProperties(...keys: (keyof T)[]): (keyof T)[] {
        return _.difference(keys, this.getDecoratedProperties());
    }

    private getDecoratedProperties(): (keyof T)[] {
        const metadata = Reflect.get(this._type, 'propDecorators');
        return _.chain(metadata)
            .keys()
            .map(key => key as keyof T)
            .value();
    }

    getDecoratedPropertyAliases() {
        const metadata = Reflect.get(this._type, 'propDecorators');
        return _.chain(this.getDecoratedProperties())
            .map(propertyName => [
                propertyName,
                metadata[propertyName][0].args?.[0] || propertyName,
            ])
            .fromPairs()
            .toPlainObject()
            .value();
    }
}
