import { Component, Input } from '@angular/core';
import { DirectiveTestBuilder } from './directive-test-builder';
import { RenderResultExtension } from './host';

describe('DirectiveTestBuilder', () => {
    @Component({
        selector: 'whatever',
        template: 'whatever',
    })
    class ComponentUnderTest {
        @Input() firstInput: any;
        @Input() secondInput: any;
    }

    let builder: DirectiveTestBuilder<ComponentUnderTest>;

    beforeEach(() => {
        builder = new DirectiveTestBuilder(ComponentUnderTest);
    });

    describe('createHost', () => {
        it('should return a promise to a _Host', async () => {
            await expect(builder.createHost()).resolves.toBeInstanceOf(
                RenderResultExtension,
            );
        });
    });
});
