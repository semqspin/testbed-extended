import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { render } from '@testing-library/angular';
import _ from 'lodash';
import { MockComponents, MockDirectives } from 'ng-mocks';
import { DetectChangesPatcher } from './detect-changes-patcher';
import { DirectiveMetadata } from './directive-metadata';
import { DeepPartial, PartialFake } from './fake-type';
import { Host, RenderResultExtension } from './host';
import { HostTypeDefinition } from './host-type-definition';
import { TestBuilder } from './test-builder';

export class DirectiveTestBuilder<T> extends TestBuilder {
    private twoWayDataBinding = false;
    private isTestDirectiveRendered = true;
    private preRenderHooks: any[] = [];
    private boundInputs: DeepPartial<T> = {} as any;
    private boundOutputs: { [key in keyof T]: jest.Mock } = {} as any;
    private patcher = new DetectChangesPatcher();

    constructor(private typeUnderTest: Type<T>) {
        super();
    }

    /**
     * @description Marks one or several keys of the component under test as bound inputs.
     *
     * Use this method if you intend to patch the value of the bound input during the test.
     * @param inputs array of keys or objects.
     *
     * If key, it must match an `@Input()` decorated property of the component class under test, and
     * it's value will be undefined when the component class is rendered.
     *
     * If object, each key of the object must be an `@Input()` decorated property of the component class under test,
     * and the corresponding value is assigned before the component class is rendered.
     *
     * @returns  current builder instance for chaining
     */
    bindInputs(...inputs: Array<keyof T | DeepPartial<T>>) {
        this.boundInputs = inputs.reduce(
            (acc: DeepPartial<T>, input) => {
                const obj: any = _.isString(input)
                    ? { [input]: undefined }
                    : input;
                return { ...(acc as any), ...obj };
            },
            { ...(this.boundInputs as object) } as DeepPartial<T>, // TODO: check if next version fixes this
        );
        return this;
    }

    /**
     * @description Marks one or several keys of the component under test as bound inputs.
     *
     * Use this method if you are interested by events emitted by the component under test
     *
     * @param outputs array of keys that must match `@Output()` decorated properties of the component class under test.
     *
     * @returns  current builder instance for chaining
     */
    bindOutputs(...outputs: Array<keyof T>) {
        this.boundOutputs = {
            ...this.boundOutputs,
            ..._(outputs)
                .map(output => [output, jest.fn()])
                .fromPairs()
                .value(),
        };
        return this;
    }

    enableTwoWayDataBinding() {
        this.twoWayDataBinding = true;
        return this;
    }

    /**
     * @description adds mocked component types the list of `declarations` in `TestBed` testing module configuration
     * @param types one or several Angular component types to mock
     * @returns  current builder instance for chaining
     */
    mockComponents(...types: Array<Type<any>>) {
        TestBed.configureTestingModule({
            declarations: MockComponents(...types),
            teardown: { destroyAfterEach: false },
        });
        return this;
    }

    /**
     * @description adds mocked directive types the list of `declarations` in `TestBed` testing module configuration
     * @param types one or several Angular directive types to mock
     * @returns  current builder instance for chaining
     */
    mockDirectives(...types: Array<Type<any>>) {
        TestBed.configureTestingModule({
            declarations: MockDirectives(...types),
            teardown: { destroyAfterEach: false },
        });
        return this;
    }

    mockProvider<P>(type: Type<P>, fakeInstance: PartialFake<P> = {}) {
        super.mockProvider(
            type,
            this.patcher.patchPropertySetters(fakeInstance),
        );
        return this;
    }

    /**
     * @description registers a callback method that will be called just after wrapping host component is rendered.
     * Useful do perform some mock service initialization required by tested directive creation.
     *
     * If called several times, all callbacks registered will be called in same order.
     * @returns  current builder instance for chaining
     */
    beforeRender(hook: (host: Host<T>) => void) {
        this.preRenderHooks = [...this.preRenderHooks, hook];
        return this;
    }

    /**
     * @description prevents the test directive to be rendered after host creation.
     * Useful do perform some mock service initialization required by tested directive creation.
     *
     * @example
     * const host = await new DirectiveTestBuilder(MyComponent)
     *      .noRenderingAtHostCreation()
     *      .createHost();
     *
     * // some initialization goes here
     *
     * await host.renderTestDirective();
     *
     * @returns  current builder instance for chaining
     */
    noRenderingAtHostCreation() {
        this.isTestDirectiveRendered = false;
        return this;
    }

    /**
     * @description generates, declares and renders a host component wrapping the component under test.
     *
     * This should be the last method invoked on a `DirectiveTestBuilder` as it terminates the builder chain.
     * @returns a promise to a `Host` offering several convenient methods to interact with the rendered component
     * @example
     * const host = await new DirectiveTestBuilder(MyComponent)
     *      .bindInputs(...)
     *      .import(...)
     *      .mockProviders(...)
     *      .createHost();
     */
    async createHost(): Promise<Host<T>> {
        const hostTypeDefinition = this.getHostTypeDefinition();
        this.declare(this.typeUnderTest, hostTypeDefinition.componentClass);
        const renderResult = await render(hostTypeDefinition.componentClass);
        const host = new RenderResultExtension(
            renderResult,
            hostTypeDefinition,
        );
        this.patcher.host = host as any;
        host.patchInputs(this.boundInputs);

        this.preRenderHooks.forEach(hook => hook(host));
        await host.renderTestDirective(this.isTestDirectiveRendered);
        return host as any;
    }

    private getHostTypeDefinition() {
        return new HostTypeDefinition(
            new DirectiveMetadata(this.typeUnderTest),
            {
                inputs: _.keys(this.boundInputs) as Array<keyof T>,
                outputs: _.keys(this.boundOutputs) as Array<keyof T>,
                detectTwoWayDataBinding: this.twoWayDataBinding,
            },
        );
    }
}
