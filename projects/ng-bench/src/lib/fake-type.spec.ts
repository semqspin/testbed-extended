import { BehaviorSubject, EMPTY, Observable, of, Subject } from 'rxjs';
import { Fake, PartialFake } from './fake-type';

describe('Fake<T> type', () => {
    /**
     * this is not specific to Angular but is very handy when it comes to mock dependencies when testing Angular components or services.
     *
     * Fake<T> is just a convenient helper to create a new type similar to T, i.e. with same public interface, but with following characteristics:
     *
     *   - all keys are optional
     *   - all keys of type functions (i.e. the class methods) are defined as jest.Mock
     *   - all keys of type Observable<S> are defined as Subject<S>
     */

    describe('mocking class public members and properties', () => {
        class SomeClass {
            id = -1;
            firstName = '';
            point = { x: 0, y: 0 };
            get age(): number {
                return 46;
            }

            // @ts-ignore
            private secret = 'shhhhh';
        }

        it('[DO] declare a variable with type "Fake<the-type-to-mock>" to get strong typing', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                id: 123,
                firstName: 'Sergio',
                age: 29,
                point: { x: 3, y: 6 },
            };
        });

        it('[DO]use PartialFake<the-type-of-mock> to define only what is needed for your test', () => {
            // @ts-ignore
            const fake: PartialFake<SomeClass> = {
                id: 123,
            };
        });

        it('Note: property with complex type are also partial', () => {
            // @ts-ignore
            const fake: PartialFake<SomeClass> = {
                point: { y: 1234 }, // no need to set the 'x' component of the point
            };
        });

        it('Note: private members are not part of the type definition', () => {
            // @ts-ignore
            const fake: PartialFake<SomeClass> = {
                // secret: 'do not tell anyone',
            };

            /**
             * uncomment causes following error:
             *
             * Object literal may only specify known properties, and 'secret' does not exist in type 'Partial<MockMethods<SomeClass>
             * & MockObservables<SomeClass> & MockOtherProperties<SomeClass>>'. ts(2322)
             */
        });
    });

    describe('mocking methods', () => {
        class SomeClass {
            // @ts-ignore
            add(a: number, b: number): number {
                throw new Error('not implemented');
            }

            createPoint(): { x: number; y: number } {
                throw new Error('not implemented');
            }

            getUser(): {
                firstName: string;
                address: { street: string; city: string };
            } {
                throw new Error('not implemented');
            }
        }

        it('[DO] assign a jest.fn() to method before it is called', () => {
            const fake: PartialFake<SomeClass> = {
                add: jest.fn(),
            };

            (fake as Fake<SomeClass>).add(12, 47);
            expect(fake.add).lastCalledWith(12, 47);

            /**
             * NOTE: the fake method has same signature, meaning that you get error if you don't respect it
             *
             *  const result2: string = fake.add('hello', 'world');
             *      => Argument of type '"hello"' is not assignable to parameter of type 'number'.
             *
             *  const result2: string = fake.add(34, 'world');
             *      => Argument of type '"world"' is not assignable to parameter of type 'number'.
             *
             *  const result2: string = fake.add(34, 56);
             *      => Type 'number' is not assignable to type 'string'.
             *
             *  const result2 = fake.add(34, 56, 78);
             *      => Expected 2 arguments, but got 3.
             */
        });

        it('[DO] mock implementation or returned value (using jest facilities) when you depend on the result of the method', () => {
            /**
             * Just pick the way you prefer, depending on your testing context.
             */
            // @ts-ignore
            const fake1: PartialFake<SomeClass> = {
                add: jest.fn().mockReturnValue(1234),
            };

            // @ts-ignore
            const fake2: PartialFake<SomeClass> = {
                // @ts-ignore
                add: jest.fn((a, b) => 1234), // note: arguments (a, b) are mandatory with this syntax
            };

            // @ts-ignore
            const fake3: PartialFake<SomeClass> = {
                add: jest.fn().mockImplementationOnce(() => 1234), // note: arguments are not mandatory here.
            };

            const fake4 = {} as Fake<SomeClass>;
            fake4.add = jest.fn();
            fake4.add.mockReturnValue(1234);
        });

        it('[DO] use jest.expect built in assertions (those with "called" in it) to verify the mock was called', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                add: jest.fn(),
            };

            fake.add(12, 34);
            fake.add(56, 78);
            fake.add(999, 111);

            expect(fake.add).toBeCalledTimes(3);
            expect(fake.add).lastCalledWith(999, 111);
            expect(fake.add).nthCalledWith(2, 56, 78); // NOTE: 1-based index !
            expect(fake.add).toBeCalledWith(12, expect.any(Number));
        });

        it('return value can be partial', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                createPoint: jest.fn(),
                getUser: jest.fn(),
            };

            fake.createPoint.mockReturnValue({ x: 10 });
            fake.getUser.mockReturnValue({ address: { city: 'Brussels' } });
        });
    });

    describe('observables', () => {
        class SomeClass {
            points$: Observable<{ x: number; y: number }> = EMPTY;
            state$: Observable<{
                value: string;
                context: {
                    users: [{ firstName: string; lastName: string }];
                    pageIndex: number;
                };
            }> = EMPTY;
        }

        it('[DO] assign a new Subject so you can control observable emitments', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                points$: new Subject(), // NOTE: no need to give generic type
            };

            fake.points$.next({ x: 12, y: 34 });
            fake.points$.next({ x: 56, y: 78 });
            fake.points$.next({ x: 99 });
            fake.points$.next({});

            /**
             * REMARK: the observable type is kept by the Fake type, meaning that you have strong
             * type checking on emitted values.
             *
             *      fake.points$.next('hello');
             *
             * causes error
             *      Type '"hello"' has no properties in common with type 'Partial<{ x: number; y: number; }>'.ts(2559)
             *
             * Partial types are accepted, so you can emit empty or partial objects, which is handy when
             * writing tests depending on services with an observable property, as you can emit just what
             * is really needed  to make your tests to pass.
             */
        });

        it('[DO] assign a new BehaviorSubject if you need a initial value as soon the observable is used', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                points$: new BehaviorSubject({ x: 12, y: 34 } as any),
            };
        });

        it('emitted values can be partial ', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                state$: new Subject(),
            };

            fake.state$.next({ context: { users: [{ firstName: 'sergio' }] } });
        });
    });

    describe('nested object properties', () => {
        class SomeClass {
            state = {
                context: {
                    user: {
                        name: 'sergio',
                        location: {
                            country: 'Belgium',
                            planet: 'Earth',
                        },
                    },
                },
            };
        }
        it('nested objects can be assigned partially as well', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                state: { context: { user: { location: { planet: 'Mars' } } } },
            };
        });
    });

    describe('arrays of objects', () => {
        class SomeClass {
            users = [
                {
                    name: 'sergio',
                    location: {
                        country: 'Belgium',
                        planet: 'Earth',
                    },
                },
                {
                    name: 'alice',
                    location: {
                        country: 'Wonderland',
                        planet: 'Imaginary',
                    },
                },
            ];
        }

        it('objects in array can be assigned partially as well', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                users: [{ location: { country: 'Italy' } }],
            };
        });
    });

    describe('some fixes', () => {
        interface OpenDialogReturnValue {
            canceled: boolean;
            filePaths: string[];
            bookmarks: string[];
        }

        class SomeClass {
            public someMethod(): Observable<any> {
                throw new Error('not implemented');
            }

            public openDialog(): Promise<OpenDialogReturnValue> {
                throw new Error('not implemented');
            }
        }

        it('should allow to return EMPTY when original return type is Observable<any>', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                someMethod: jest.fn(() => EMPTY),
            };
        });

        it('should allow to return Observable<string> when original return type is Observable<any>', () => {
            // @ts-ignore
            const fake: Fake<SomeClass> = {
                someMethod: jest.fn(() => of('hello')),
            };
        });
    });
});
