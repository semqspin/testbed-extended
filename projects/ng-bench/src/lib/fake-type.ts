// tslint:disable: no-shadowed-variable
import { EventEmitter } from '@angular/core';
import { Observable, Subject } from 'rxjs';

type Primitive = string | number | boolean | symbol;

// prettier-ignore
type FakeType<T> = 
    T extends (...args: any[]) => any ?  jest.Mock<SmartPartial<ReturnType<T>>, Parameters<T>> :
    T extends Observable<infer U> ? Subject<SmartPartial<U>> :
    T extends Promise<infer U> ? Promise<SmartPartial<U>> :
    T extends Primitive ? T :     
    T extends any[] ? Array<SmartPartial<T[number]>> : 
    T extends object ? SmartPartial<T> :
    T;

// prettier-ignore
export type DeepPartial<T> = 
    T extends Primitive ? T : {
          -readonly [P in keyof T]?: SmartPartial<T[P]>;
    };

// prettier-ignore
type SmartPartial<T> = 
    T extends Observable<infer U> ? Observable<DeepPartial<U>> :
    T extends Promise<infer U> ? Promise<DeepPartial<U>> :
    T extends Primitive ? T :     
    T extends any[] ? Array<DeepPartial<T[number]>> : 
    T extends object ? DeepPartial<T> :
    T;

export type Fake<T> = {
    -readonly [P in keyof T]: FakeType<T[P]>;
};

export type PartialFake<T> = Partial<Fake<T>>;

export type OutputEvent<T> = {
    [key in keyof T]: T[key] extends EventEmitter<infer U> ? U : never;
};

export type OutputEvents<T> = {
    [key in keyof T]: T[key] extends EventEmitter<infer U> ? Array<U> : never;
};
