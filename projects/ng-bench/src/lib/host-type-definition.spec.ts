import { Component, Type } from '@angular/core';
import _ from 'lodash';
import { DirectiveMetadata } from './directive-metadata';
import { Fake, PartialFake } from './fake-type';
import { HostTypeDefinition, IHostComponent } from './host-type-definition';

describe('HostTypeDefinition', () => {
    let fakeInspector: Fake<DirectiveMetadata<any>>;
    beforeEach(() => {
        fakeInspector = ({
            type: class SomeDirective {},
            getSelector: jest.fn(() => 'default-selector'),
            getDecoratedPropertyAliases: jest.fn().mockReturnValue({}),
            getNonDecoratedProperties: jest.fn().mockReturnValue({}),
        } as PartialFake<DirectiveMetadata<any>>) as Fake<
            DirectiveMetadata<any>
        >;
    });

    describe('inputs', () => {
        it('should return empty array when no binding inputs passed at construction', () => {
            const hostDefinition = new HostTypeDefinition(fakeInspector as any);
            expect(hostDefinition.inputs).toEqual([]);
        });

        it('should return same inputs as those passed at construction', () => {
            fakeInspector.getDecoratedPropertyAliases.mockReturnValue({
                prop1: 'prop1',
                prop2: 'prop2',
            });
            const hostDefinition = new HostTypeDefinition(
                fakeInspector as any,
                {
                    inputs: ['prop1', 'prop2'],
                },
            );
            expect(hostDefinition.inputs).toIncludeSameMembers([
                'prop1',
                'prop2',
            ]);
        });

        it('should return different instances at each call', () => {
            const hostDefinition = new HostTypeDefinition(fakeInspector as any);
            expect(hostDefinition.inputs).not.toBe(hostDefinition.inputs);
        });
    });

    describe('outputs', () => {
        it('should return empty array when no binding outputs passed at construction', () => {
            const hostDefinition = new HostTypeDefinition(fakeInspector as any);
            expect(hostDefinition.outputs).toEqual([]);
        });

        it('should return same outputs as those passed at construction', () => {
            fakeInspector.getDecoratedPropertyAliases.mockReturnValue({
                prop1: 'prop1',
                prop2: 'prop2',
            });
            const hostDefinition = new HostTypeDefinition(
                fakeInspector as any,
                {
                    outputs: ['prop1', 'prop2'],
                },
            );
            expect(hostDefinition.outputs).toIncludeSameMembers([
                'prop1',
                'prop2',
            ]);
        });

        it('should return different instances at each call', () => {
            const hostDefinition = new HostTypeDefinition(fakeInspector as any);
            expect(hostDefinition.outputs).not.toBe(hostDefinition.inputs);
        });
    });

    describe('componentClass (getter)', () => {
        let hostComponentClass: Type<IHostComponent>;

        beforeEach(() => {
            fakeInspector.getDecoratedPropertyAliases.mockReturnValue({
                prop1: 'prop1',
            });
            const hostTypeDefinition = new HostTypeDefinition(
                fakeInspector as any,
                { outputs: ['prop1'] },
            );
            hostComponentClass = hostTypeDefinition.componentClass;
        });

        it('should return a class', () => {
            // NOTE: test it is a class by creating an instance
            expect(() => new hostComponentClass()).not.toThrow();
        });

        it('instances of the class should have property "isTestComponentShown" with value "false"', () => {
            const instance = new hostComponentClass();
            expect(instance.isTestComponentShown).toBeFalse();
        });

        it('instances of the class should have one property for each output with empty array as value', () => {
            const instance = new hostComponentClass();
            expect(instance).toHaveProperty('prop1', []);
        });

        describe('component annotation', () => {
            let annotations: any[];
            beforeEach(() => {
                annotations = (hostComponentClass as any).__annotations__;
            });

            it('class should be decorated by component annotation', () => {
                expect(_.first(annotations)).toBeInstanceOf(Component);
            });

            it('component should have template', () => {
                const component = _.first(annotations) as Component;
                expect(component.template).toBeString();
            });
        });
    });

    describe('template', () => {
        const getTemplateFrom = (hostDefinition: HostTypeDefinition<any>) => {
            const componentClass = hostDefinition.componentClass;
            const annotations = (componentClass as any).__annotations__;
            const component = _.first(annotations) as Component;
            return component.template;
        };

        describe('element template', () => {
            beforeEach(() => {
                fakeInspector.getSelector.mockReturnValue('my-selector');
            });

            it('should be an element with selector of the wrapped component', () => {
                const hostDefinition = new HostTypeDefinition(
                    fakeInspector as any,
                );
                expect(getTemplateFrom(hostDefinition)).toBe(
                    '<my-selector *ngIf="isTestComponentShown"></my-selector>',
                );
            });

            it('should contain inputs and outputs', () => {
                fakeInspector.getDecoratedPropertyAliases.mockReturnValue({
                    prop1: 'alias1',
                    prop2: 'alias2',
                    prop3: 'alias3',
                    prop4: 'alias4',
                });
                const hostDefinition = new HostTypeDefinition(
                    fakeInspector as any,
                    {
                        inputs: ['prop1', 'prop2'],
                        outputs: ['prop3', 'prop4'],
                    },
                );
                const template = getTemplateFrom(hostDefinition);
                expect(template).toMatch(' [alias1]="prop1"');
                expect(template).toMatch(' [alias2]="prop2"');
                expect(template).toMatch(' (alias3)="prop3.push($event)"');
                expect(template).toMatch(' (alias4)="prop4.push($event)"');
            });
        });
        describe('attribute template (when selector surrounded by square brackets)', () => {
            it('should be a div with attribute when selector is surrounded by square bracketts', () => {
                fakeInspector.getSelector.mockReturnValue('[myDirective]');
                const hostDefinition = new HostTypeDefinition(
                    fakeInspector as any,
                );
                expect(getTemplateFrom(hostDefinition)).toBe(
                    `<div myDirective *ngIf="isTestComponentShown"></div>`,
                );
            });

            it('should contain inputs and outputs', () => {
                fakeInspector.getSelector.mockReturnValue('[myDirective]');
                fakeInspector.getDecoratedPropertyAliases.mockReturnValue({
                    prop1: 'alias1',
                    prop2: 'alias2',
                    prop3: 'alias3',
                    prop4: 'alias4',
                });
                const hostDefinition = new HostTypeDefinition(
                    fakeInspector as any,
                    {
                        inputs: ['prop1', 'prop2'],
                        outputs: ['prop3', 'prop4'],
                    },
                );
                const template = getTemplateFrom(hostDefinition);
                expect(template).toMatch('<div myDirective');
                expect(template).toMatch(' [alias1]="prop1"');
                expect(template).toMatch(' [alias2]="prop2"');
                expect(template).toMatch(' (alias3)="prop3.push($event)"');
                expect(template).toMatch(' (alias4)="prop4.push($event)"');
            });

            it('should ', () => {
                fakeInspector.getSelector.mockReturnValue('[myDirective]');
                fakeInspector.getDecoratedPropertyAliases.mockReturnValue({
                    prop1: 'myDirective',
                });
                const hostDefinition = new HostTypeDefinition(
                    fakeInspector as any,
                    {
                        inputs: ['prop1'],
                    },
                );
                const template = getTemplateFrom(hostDefinition);
                expect(template).not.toMatch('<div myDirective');
                expect(template).toMatch(' [myDirective]="prop1"');
            });
        });
    });

    describe('check consistence', () => {
        it('should throw when some inputs are not binding properties', () => {
            const inputs = ['prop1', 'prop2', 'prop3'];
            const outputs = ['prop4', 'prop5', 'prop6'];
            fakeInspector.directiveName = 'the-directive-name';

            fakeInspector.getNonDecoratedProperties.mockImplementation(
                (...bindings) =>
                    _.isEqual(bindings, inputs)
                        ? ['prop1', 'prop3']
                        : ['prop5', 'prop6'],
            );
            expect(
                () =>
                    new HostTypeDefinition(fakeInspector as any, {
                        inputs,
                        outputs,
                    }),
            ).toThrow(
                [
                    `Invalid bindings for component "the-directive-name"`,
                    `  - "prop1" has no @Input() decorator`,
                    `  - "prop3" has no @Input() decorator`,
                    `  - "prop5" has no @Output() decorator`,
                    `  - "prop6" has no @Output() decorator`,
                ].join('\n'),
            );
        });
    });
});
