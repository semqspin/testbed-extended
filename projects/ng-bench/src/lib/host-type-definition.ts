import { Component, Type } from '@angular/core';
import _ from 'lodash';
import 'reflect-metadata';
import { DirectiveMetadata } from './directive-metadata';

export interface IHostComponent {
    isTestComponentShown: boolean;
}

export interface IBindingOptions<T = any> {
    inputs: (keyof T)[];
    outputs: (keyof T)[];
    detectTwoWayDataBinding?: boolean;
}

export class HostTypeDefinition<T> {
    private hostClass: Type<IHostComponent>;
    private bindingOptions: IBindingOptions<T>;

    constructor(
        private directiveMetadata: DirectiveMetadata<T>,
        bindingOptions: Partial<IBindingOptions<T>> = {},
    ) {
        this.bindingOptions = this.sanitizeOptions(bindingOptions);
        this.checkBindingNames();
        this.hostClass = this.defineHostComponentClass();
    }

    get testedDirectiveType(): Type<T> {
        return this.directiveMetadata.type;
    }

    get componentClass(): Type<IHostComponent> {
        return this.hostClass;
    }

    public get inputs(): (keyof T)[] {
        return [...this.bindingOptions.inputs];
    }

    public get outputs(): (keyof T)[] {
        return [...this.bindingOptions.outputs];
    }

    private sanitizeOptions(bindingOptions: Partial<IBindingOptions<T>>) {
        return {
            inputs: [],
            outputs: [],
            detectTwoWayDataBinding: false,
            ..._.cloneDeep(bindingOptions),
        };
    }

    private checkBindingNames() {
        if (this.atLeastOneInvalidBinding) {
            throw new Error(
                [
                    `Invalid bindings for component "${this.directiveMetadata.directiveName}"`,
                    ...this.invalidInputMessages,
                    ...this.invalidOutputMessages,
                ].join('\n'),
            );
        }
    }
    private get atLeastOneInvalidBinding(): boolean {
        return this.invalidInputs.length > 0 || this.invalidOutputs.length > 0;
    }

    private get invalidInputs() {
        return this.directiveMetadata.getNonDecoratedProperties(
            ...this.bindingOptions.inputs,
        );
    }
    private get invalidOutputs() {
        return this.directiveMetadata.getNonDecoratedProperties(
            ...this.bindingOptions.outputs,
        );
    }

    private get invalidInputMessages(): string[] {
        return _(this.invalidInputs)
            .sort()
            .map(prop => this.toBindingErrorMessage(prop, '@Input'))
            .value();
    }

    private get invalidOutputMessages(): string[] {
        return _(this.invalidOutputs)
            .sort()
            .map(prop => this.toBindingErrorMessage(prop, '@Output'))
            .value();
    }

    private toBindingErrorMessage(
        prop: keyof T,
        missingDecorator: '@Input' | '@Output',
    ) {
        return `  - "${prop}" has no ${missingDecorator}() decorator`;
    }

    private defineHostComponentClass() {
        this.defineHostClass();
        this.decorateClassAsAngularComponent();
        return this.hostClass;
    }

    private defineHostClass() {
        const typeDefinition = this;
        this.hostClass = class implements IHostComponent {
            isTestComponentShown = false;
            constructor() {
                typeDefinition.outputs.forEach(output => {
                    (this as any)[output as string] = [];
                });
            }
        };
    }

    private decorateClassAsAngularComponent() {
        Reflect.decorate(
            [Component({ template: this.getTemplate() })],
            this.hostClass,
        );
    }

    private getTemplate(): string {
        return this.isAttributeSelector
            ? this.getTemplateWithSelectorAsAttribute()
            : this.getTemplateWithSelectorAsElement();
    }

    private get isAttributeSelector(): boolean {
        return !!this.directiveMetadata.getSelector().match(/^\[(.*)\]$/);
    }

    private getTemplateWithSelectorAsElement() {
        const selector = this.directiveMetadata.getSelector();
        return [
            `<${selector} *ngIf="isTestComponentShown"`,
            this.templateBindings,
            `></${selector}>`,
        ].join('');
    }

    private getTemplateWithSelectorAsAttribute() {
        return [
            `<div`,
            this.selectorAsAttribute,
            this.templateBindings,
            ` *ngIf="isTestComponentShown"></div>`,
        ].join('');
    }

    private get selectorAsAttribute() {
        const selectorWithoutBrackets = this.selectorWithoutBrackets;

        return _(this.directiveMetadata.getDecoratedPropertyAliases())
            .values()
            .includes(this.selectorWithoutBrackets)
            ? ''
            : ` ${selectorWithoutBrackets}`;
    }

    private get selectorWithoutBrackets() {
        const matchResults = this.directiveMetadata
            .getSelector()
            .match(/^\[(.*)\]$/);

        return _.nth(matchResults, 1);
    }

    private get templateBindings(): string {
        const res = [
            ...this.inputBindings,
            ...this.outputBindings,
            ...this.twoWayBindings,
        ].join(' ');
        return _.isEmpty(res) ? res : ` ${res}`;
    }

    private get inputBindings(): string[] {
        return this.mapBindingsToTemplateContent(
            this.oneWayInputs,
            ([input, alias]) => `[${alias}]="${input}"`,
        );
    }

    private get outputBindings(): string[] {
        return this.mapBindingsToTemplateContent(
            this.oneWayOutputs,
            ([output, alias]) => `(${alias})="${output}.push($event)"`,
        );
    }

    private get twoWayBindings(): string[] {
        return this.mapBindingsToTemplateContent(
            this.twoWayDataBindingProperties,
            ([input, alias]) => `
                    [${alias}]="${input}" 
                    (${alias}Change)="${input}=$event; ${input}Change.push($event)"
                `,
        );
    }

    private get oneWayInputs(): (keyof T)[] {
        return _.difference(this.inputs, this.twoWayDataBindingProperties);
    }

    private get oneWayOutputs(): (keyof T)[] {
        return _.difference(
            this.outputs,
            this.twoWayDataBindingProperties.map(key => `${key}Change` as any),
        );
    }

    private get twoWayDataBindingProperties() {
        return this.bindingOptions.detectTwoWayDataBinding
            ? this.bindingOptions.inputs.filter(key =>
                  this.bindingOptions.outputs.includes(`${key}Change` as any),
              )
            : [];
    }

    private mapBindingsToTemplateContent(
        bindings: (keyof T)[],
        fn: ([bindingName, alias]: [keyof T, string]) => string,
    ): string[] {
        const aliases = this.directiveMetadata.getDecoratedPropertyAliases();
        return bindings
            .map<[keyof T, string]>(input => [input, aliases[input]])
            .map(fn);
    }
}
