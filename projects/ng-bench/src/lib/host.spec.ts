import { fireEvent, RenderResult } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import _ from 'lodash';
import { Host, RenderResultExtension } from './host';
import { HostTypeDefinition, IHostComponent } from './host-type-definition';

describe('_Host<T>', () => {
    it('should create', () => {
        const host = new RenderResultExtension(
            {} as RenderResult<any>,
            {} as HostTypeDefinition<any>,
        );
        expect(host).toBeInstanceOf(RenderResultExtension);
    });

    it('should be a proxy to all members exposed by provided renderResult', () => {
        const fakeRenderResult = { SomeMethodOfRenderResult: jest.fn() };
        const host = new RenderResultExtension(
            (fakeRenderResult as any) as RenderResult<IHostComponent>,
            {} as HostTypeDefinition<any>,
        ) as any;
        host.SomeMethodOfRenderResult('hello');
        expect(fakeRenderResult.SomeMethodOfRenderResult).lastCalledWith(
            'hello',
        );
    });

    it('should be a proxy to all props exposed by fireEvent', () => {
        const fireEventMouseDownSpy = jest
            .spyOn(fireEvent, 'mouseDown')
            .mockImplementation(_.noop as any);
        const host = new RenderResultExtension(
            {} as RenderResult<IHostComponent>,
            {} as HostTypeDefinition<any>,
        ) as Host<any>;
        host.mouseDown('the-node' as any, 'the-options' as any);
        expect(fireEventMouseDownSpy).lastCalledWith('the-node', 'the-options');
    });

    it('should be a proxy to all props exposed by userEvent', () => {
        const userEventMouseDownSpy = jest
            .spyOn(userEvent, 'type')
            .mockImplementation(_.noop as any);
        const host = new RenderResultExtension(
            {} as RenderResult<IHostComponent>,
            {} as HostTypeDefinition<any>,
        ) as Host<any>;
        host.type('the-element' as any, 'the-text', 'the-options' as any);
        expect(userEventMouseDownSpy).lastCalledWith(
            'the-element',
            'the-text',
            'the-options',
        );
    });

    describe('detectChanges', () => {
        interface FakeRenderResult {
            detectChanges: jest.Mock;
        }
        let fakeRenderResult: FakeRenderResult;
        let host: RenderResultExtension<any>;

        beforeEach(() => {
            fakeRenderResult = { detectChanges: jest.fn() };
            host = new RenderResultExtension(
                (fakeRenderResult as any) as RenderResult<IHostComponent>,
                {} as HostTypeDefinition<any>,
            );
        });

        it('should delegate to proxied RenderResult', () => {
            host.detectChanges();
            expect(fakeRenderResult.detectChanges).toBeCalled();
        });

        it('accepts a setup function which is called before detectChanges is invoked', () => {
            const setupFn = jest.fn();
            host.detectChangesOnceAfter(setupFn);
            expect(setupFn).toHaveBeenCalledBefore(
                fakeRenderResult.detectChanges,
            );
        });

        it('all detectChanges caused during setupFn are ignored, and detectChanges is called exactly once', () => {
            const someFunctionTriggeringDetectChanges = () =>
                host.detectChanges();
            const setupFn = jest.fn(() => {
                someFunctionTriggeringDetectChanges();
            });
            host.detectChangesOnceAfter(setupFn);
            expect(fakeRenderResult.detectChanges).toBeCalledTimes(1);
        });

        it('should not leave host in a pending state if setup function throws', () => {
            const setupFn = jest.fn().mockImplementationOnce(() => {
                throw new Error('Boom');
            });

            expect(() => host.detectChangesOnceAfter(setupFn)).toThrow('Boom');
            expect(fakeRenderResult.detectChanges).not.toBeCalled();

            host.detectChanges();
            expect(fakeRenderResult.detectChanges).toBeCalled();
        });
    });

    describe('getAllByDirective', () => {
        const debugElement = (value: number) =>
            _({})
                .set('injector.get', jest.fn().mockReturnValue(value))
                .set('nativeElement', value)
                .value();

        let fakeRenderResult: any;
        let host: RenderResultExtension<any>;

        beforeEach(() => {
            fakeRenderResult = _({})
                .set(
                    'fixture.debugElement.queryAll',
                    jest
                        .fn()
                        .mockReturnValue([
                            debugElement(123),
                            debugElement(456),
                            debugElement(789),
                        ]),
                )
                .value();

            host = new RenderResultExtension(
                (fakeRenderResult as any) as RenderResult<IHostComponent>,
                {} as HostTypeDefinition<any>,
            );
        });

        it('should filter elements according to predicate', () => {
            const res = host.getAllByDirective(Number, (it: any) => it > 400);
            expect(res).toEqual([456, 789]);
        });

        it('should return all elements when no predicate is provided', () => {
            const res = host.getAllByDirective(Number);
            expect(res).toEqual([123, 456, 789]);
        });
    });
});
