import { DebugElement, EventEmitter, Predicate, Type } from '@angular/core';
import { By } from '@angular/platform-browser';
import { fireEvent, RenderResult } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import _ from 'lodash';
import { DetectChangesPatcher } from './detect-changes-patcher';
import { DeepPartial, OutputEvent, OutputEvents } from './fake-type';
import { HostTypeDefinition, IHostComponent } from './host-type-definition';

const MISSING_BINDING_TIP_MESSAGE =
    'Did you forget to register some bindings for tested element?';

export class RenderResultExtension<T> {
    private patcher = new DetectChangesPatcher();
    private insideDetectChanges = 0;

    // alias
    renderTestComponent = this.renderTestDirective;

    constructor(
        private renderResult: RenderResult<IHostComponent>,
        private hostTypeDefinition: HostTypeDefinition<T>,
    ) {
        const proxy = new Proxy(this, {
            get(target, prop) {
                // we want the proxy to combine all the properties from RenderResult<IHostComponent>, fireEvent and userEvent;
                return (
                    (target as any)[prop] ??
                    (userEvent as any)[prop] ??
                    (fireEvent as any)[prop] ??
                    (renderResult as any)[prop]
                );
            },
        });

        this.patcher.host = proxy as any;

        return proxy;
    }

    /**
     * The containing DOM node of the Angular Component currently under test.
     * This is a regular DOM node, so you can call container.querySelector etc. to inspect the children.
     */
    get container(): HTMLElement {
        // "overwrite" the original container property
        return this.testedDebugElement.nativeElement;
    }

    /**
     * The containing DOM node of the Angular Component wrapping the component under test
     * This is a regular DOM node, so you can call container.querySelector etc. to inspect the children.
     */
    get parentContainer(): HTMLElement {
        return this.renderResult.container as HTMLElement;
    }

    /**
     * @description
     * Prints out the tested component's DOM with syntax highlighting.
     * Accepts an optional parameter, to print out a specific DOM node.
     *
     * @param
     * element: The DOM element to print
     */
    debug(element: Element | HTMLDocument = this.container) {
        this.renderResult.debug(element ?? this.container);
    }

    /**
     * @description
     * Trigger a change detection cycle for the component.
     * @param [setupFn] a function invoked before triggering the change detection.
     * All change detection triggered during setupFn call are ignored, so that detectChanges is only triggered once
     */
    detectChanges() {
        if (this.insideDetectChanges > 0) {
            return;
        }
        this.renderResult.detectChanges();
    }

    detectChangesOnceAfter(setupFn: () => any) {
        this.insideDetectChanges += 1;
        const err = this.captureErrorSilently(setupFn);
        this.insideDetectChanges -= 1;
        if (err) {
            throw err;
        } else {
            this.detectChanges();
        }
    }

    private captureErrorSilently(setupFn: () => any): Error | null {
        try {
            setupFn();
            return null;
        } catch (error: any) {
            return error;
        }
    }

    async whenStable() {
        return this.renderResult.fixture.whenStable();
    }

    get debugElement(): DebugElement {
        return this.renderResult.fixture.debugElement;
    }

    async renderTestDirective(show = true) {
        this.hostInstance.isTestComponentShown = show;
        this.renderResult.detectChanges();
        await this.whenStable();
        this.renderResult.detectChanges();
    }

    async resetTestComponent(setup: () => void = _.noop) {
        await this.renderTestDirective(false);
        setup();
        await this.renderTestDirective(true);
    }

    async destroyTestComponent() {
        await this.renderTestComponent(false);
    }

    /**
     * Gets instance of the host component currently wrapping the component under test
     *
     * NOTE: consider `testInstance` to get instance of the component under test
     */
    get hostInstance(): IHostComponent & Partial<T> {
        return this.renderResult.fixture.componentInstance as any;
    }

    private get testedDebugElement(): DebugElement {
        return this.debugElement.query(
            By.directive(this.hostTypeDefinition.testedDirectiveType),
        );
    }

    /**
     * Gets instance of the component under test
     *
     * NOTE: consider `hostInstance` to get instance of the component wrapping the component under test
     */
    get testInstance(): T {
        return this.testedDebugElement.componentInstance;
    }

    /**
     * patches one or several bound property values and trigger detect changes
     * @param input an object where each key is a registered bound input and value is the
     * new value to assign to the corresponding bound input
     */
    patchInputs(input: DeepPartial<T>) {
        this.checkAllKeysMatchInputProperties(_.keys(input) as (keyof T)[]);
        Object.assign(this.hostInstance, input);
        this.detectChanges();
        return input;
    }

    private checkAllKeysMatchInputProperties(keys: (keyof T)[]) {
        const invalidKeys = _.difference(keys, this.hostTypeDefinition.inputs);
        if (invalidKeys.length > 0) {
            const sortedKeys = _(invalidKeys).sort().join(', ');
            throw new Error(
                [
                    `Following keys are not registered as component inputs: ${sortedKeys}.`,
                    MISSING_BINDING_TIP_MESSAGE,
                ].join('\n'),
            );
        }
    }

    /**
     * @description get array with all events value emitted by a bound output
     * @param output the name of the bound output
     * @returns an array contaning all events emitted so far. If new events are emitted
     * after the array is returned, new values will be pushed to that array.
     */
    getOutputEvents<K extends keyof OutputEvents<T>>(
        output: K,
    ): OutputEvents<T>[K] {
        this.checkOutputMatchOutputProperties(output);
        return (this.hostInstance as any)[output];
    }

    /**
     * @param output the name of the bound output
     * @returns get latest event value emitted by a bound output
     */
    latestOutputEvent<K extends keyof OutputEvents<T>>(
        output: K,
    ): OutputEvent<T>[K] {
        const allEvents = this.getOutputEvents(output);
        if (_.isEmpty(allEvents)) {
            throw new Error(`Output event "${output}" was never emitted.`);
        }
        return _.last(allEvents) as OutputEvent<T>[K];
    }

    private checkOutputMatchOutputProperties(key: keyof T) {
        if (!this.hostTypeDefinition.outputs.includes(key)) {
            throw new Error(
                [
                    `Key "${key}" is not registered as component output`,
                    MISSING_BINDING_TIP_MESSAGE,
                ].join('\n'),
            );
        }
    }

    queryInstanceByDirective<D>(
        type: Type<D>,
        predicate: Predicate<D> = () => true,
    ): D | null {
        const mockedDirectives = this.getAllInstancesByDirective(
            type,
            predicate,
        );
        if (mockedDirectives.length > 1) {
            throw new Error(
                `Found multiple directives of type "${type.name}". If this is intentional, use the 'getAllInstancesByDirective' variant`,
            );
        }
        return mockedDirectives.length === 1 ? mockedDirectives[0] : null;
    }

    getInstanceByDirective<D>(
        type: Type<D>,
        predicate: Predicate<D> = () => true,
    ): D {
        const mockedDirectives = this.getAllInstancesByDirective(
            type,
            predicate,
        );
        if (mockedDirectives.length !== 1) {
            throw new Error(
                `Expected unique instance for directive of type "${type.name}" but found ${mockedDirectives.length} instance(s)`,
            );
        }
        return _.first(mockedDirectives) as D;
    }

    getAllInstancesByDirective<D>(
        type: Type<D>,
        predicate: Predicate<D> = () => true,
    ): D[] {
        return this.getAllDebugElementsByDirective(type)
            .map(debugElement => debugElement.injector.get(type))
            .filter(predicate)
            .map(instance => this.patchEventEmitter(instance));
    }

    private patchEventEmitter<D>(instance: any): D {
        _(instance)
            .filter(prop => prop instanceof EventEmitter)
            .forEach((outputEmitter: EventEmitter<any>) => {
                this.patcher.patchMethodInvocation(outputEmitter, 'emit');
            });

        return instance;
    }

    queryByDirective<D>(
        type: Type<D>,
        predicate: Predicate<D> = () => true,
    ): HTMLElement | null {
        const mockedDirectives = this.getAllByDirective(type, predicate);
        if (mockedDirectives.length > 1) {
            throw new Error(
                `Found multiple directives of type "${type.name}". If this is intentional, use the 'getAllByDirective' variant`,
            );
        }
        return mockedDirectives.length === 1 ? mockedDirectives[0] : null;
    }

    getByDirective<D>(
        type: Type<D>,
        predicate: Predicate<D> = () => true,
    ): HTMLElement {
        const mockedDirectives = this.getAllByDirective(type, predicate);
        if (mockedDirectives.length !== 1) {
            throw new Error(
                `Expected unique HTML element for directive of type "${type.name}" but found ${mockedDirectives.length} HTML element(s)`,
            );
        }
        return _.first(mockedDirectives) as HTMLElement;
    }

    getAllByDirective<D>(
        type: Type<D>,
        predicate: Predicate<D> = () => true,
    ): HTMLElement[] {
        return this.getAllDebugElementsByDirective(type)
            .filter(debugElement => predicate(debugElement.injector.get(type)))
            .map(debugElement => debugElement.nativeElement);
    }

    private getAllDebugElementsByDirective<D>(type: Type<D>): DebugElement[] {
        return this.debugElement.queryAll(By.directive(type));
    }
}

/**
 * @description cool
 */
export type Host<T> = RenderResultExtension<T> &
    RenderResult<IHostComponent> &
    typeof fireEvent &
    typeof userEvent;
