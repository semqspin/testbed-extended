import { Injectable } from '@angular/core';
import { fake } from '../public-api';
import { InjectableTestBuilder } from './injectable-test-builder';

describe('InjectableTestBuilder', () => {
    describe('basic usage', () => {
        class ServiceUnderTest {}

        it('should build an instance of the service under test', () => {
            const service = new InjectableTestBuilder(
                ServiceUnderTest,
            ).createInstance();
            expect(service).toBeInstanceOf(ServiceUnderTest);
        });
    });

    describe('inject dependencies', () => {
        class DependencyOne {
            someProperty = 1234;
            someMethod() {}
        }
        class DependencyTwo {}

        @Injectable()
        class ServiceUnderTest {
            constructor(
                public dep1: DependencyOne,
                public dep2: DependencyTwo,
            ) {}
        }

        it('should throw if missing dependency', () => {
            expect(() =>
                new InjectableTestBuilder(ServiceUnderTest).createInstance(),
            ).toThrow('NullInjectorError: No provider for DependencyOne!');
        });

        describe('mockProviders', () => {
            it('should provide empty objects for registered classes', () => {
                new InjectableTestBuilder(ServiceUnderTest).mockProviders(
                    DependencyOne,
                    DependencyTwo,
                );

                expect(fake(DependencyOne)).toEqual({});
                expect(fake(DependencyTwo)).toEqual({});
            });

            it('should inject fake instances to service under test once built', () => {
                const service = new InjectableTestBuilder(ServiceUnderTest)
                    .mockProviders(DependencyOne, DependencyTwo)
                    .createInstance();

                expect(service.dep1).toBe(fake(DependencyOne));
                expect(service.dep2).toBe(fake(DependencyTwo));
            });
        });

        describe('mockProvider', () => {
            it('should provide empty objects for registered classes', () => {
                new InjectableTestBuilder(ServiceUnderTest).mockProvider(
                    DependencyOne,
                );

                expect(fake(DependencyOne)).toEqual({});
            });

            it('should inject fake instances to service under test once built', () => {
                const service = new InjectableTestBuilder(ServiceUnderTest)
                    .mockProvider(DependencyOne)
                    .mockProvider(DependencyTwo)
                    .createInstance();

                expect(service.dep1).toBe(fake(DependencyOne));
                expect(service.dep2).toBe(fake(DependencyTwo));
            });

            it('[DO] use optional second argument to initialize fake service if needed', () => {
                new InjectableTestBuilder(ServiceUnderTest).mockProvider(
                    DependencyOne,
                    {
                        someProperty: 9999,
                    },
                );

                expect(fake(DependencyOne).someProperty).toBe(9999);
            });
        });
    });
});
