import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { TestBuilder } from './test-builder';

export class InjectableTestBuilder<T> extends TestBuilder {
    constructor(private type: Type<T>) {
        super();
        this.provide(type);
    }

    /**
     * @returns instance of the type under test provided by Angular dependency injection mechanism.
     */
    createInstance(): T {
        return TestBed.get(this.type);
    }
}
