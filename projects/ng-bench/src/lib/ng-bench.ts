import { Type } from '@angular/core';
import { DirectiveTestBuilder } from './directive-test-builder';
import { InjectableTestBuilder } from './injectable-test-builder';

/**
 * static class with methods to configure Angular context for tests
 */
export class NgBench {
    /**
     * @description creates a `DirectiveTestBuilder` to configure Angular context when testing an Angular component
     * @param type type of the Angular component under test
     * @returns  `DirectiveTestBuilder` for the component class under test
     * @example
     * NgBench.testComponent(MyComponent)
     */
    static testComponent<T>(type: Type<T>) {
        return new DirectiveTestBuilder(type);
    }

    /**
     * @description creates a `InjectableTestBuilder` to configure Angular context when testing an Angular injectable service
     * @param type type of the Angular service under test
     * @returns  `InjectableTestBuilder`
     * @example
     * NgBench.testInjectable(MyService)
     */
    static testInjectable<T>(type: Type<T>) {
        return new InjectableTestBuilder(type);
    }
}
