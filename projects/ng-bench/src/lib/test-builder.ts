import { Pipe, PipeTransform, Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MockedModule, MockPipe } from 'ng-mocks';
import { Fake, PartialFake } from './fake-type';

export abstract class TestBuilder {
    /**
     * @description adds modules to the list of `imports` in `TestBed` testing module configuration
     * @returns  current builder instance for chaining
     */
    import(...modules: Array<Type<any> | MockedModule<any>>) {
        TestBed.configureTestingModule({
    imports: modules,
    teardown: { destroyAfterEach: false }
});
        return this;
    }

    /**
     * @description adds types to the list of `declarations` in `TestBed` testing module configuration
     * @returns  current builder instance for chaining
     */
    declare(...types: Array<Type<any>>) {
        TestBed.configureTestingModule({
    declarations: types,
    teardown: { destroyAfterEach: false }
});
        return this;
    }

    /**
     * @description adds providers to the list of `providers` in `TestBed` testing module configuration
     * @returns  current builder instance for chaining
     */
    provide(...providers: any[]) {
        TestBed.configureTestingModule({
    providers,
    teardown: { destroyAfterEach: false }
});
        return this;
    }

    /**
     * @description adds providers to the list of `providers` in `TestBed` testing module configuration.
     *
     * Each provider injects an empty object as fake instance for the provided type.
     *
     * Once element under test created, use `fake(type)` to retrieve the fake instance provided for that type
     * @returns  current builder instance for chaining
     */
    mockProviders(...types: Array<Type<any>>) {
        types.forEach(type => this.provideOneFake(type, {}));
        return this;
    }

    /**
     * @description adds one provider to the list of `providers` in `TestBed` testing module configuration.
     *
     * Once element under test created, use `fake(type)` to retrieve the fake instance provided for that type
     *
     * @param [instance] an optional setup function or object to initialize the fake instance; if `null`
     * an empty object will be injected as fake instance for the provided type
     *
     * @returns  current builder instance for chaining
     */
    mockProvider<P>(type: Type<P>, instance: PartialFake<P> = {}) {
        this.provideOneFake(type, instance);
        return this;
    }

    private provideOneFake(type: Type<any>, fakeInstance: Fake<any>) {
        TestBed.configureTestingModule({
    providers: [{ provide: type, useValue: fakeInstance }],
    teardown: { destroyAfterEach: false }
});
        TestBed.overrideProvider(type, { useValue: fakeInstance });

        return fakeInstance;
    }

    /**
     * @description adds mocked pipes the list of `declarations` in `TestBed` testing module configuration
     * @param types one or several Angular pipe types to mock
     * @returns  current builder instance for chaining
     */
    mockPipes(...types: Array<Type<PipeTransform> | string>) {
        types.forEach(type => this.mockPipe(type));
        return this;
    }

    mockPipe(
        pipe: Type<PipeTransform> | string,
        transform?: (value: any, ...args: any[]) => any,
    ) {
        if (typeof pipe === 'string') {
            pipe = Reflect.decorate([Pipe({ name: pipe })], class {}) as Type<
                PipeTransform
            >;
        }

        transform =
            transform ||
            this.getDefaultPipeTransformImplementation(this.getPipeName(pipe));

        TestBed.configureTestingModule({
    declarations: [MockPipe(pipe, transform)],
    teardown: { destroyAfterEach: false }
});

        this.provideOneFake(pipe, { transform });
        return this;
    }

    private getDefaultPipeTransformImplementation(pipeName: string) {
        return (value: any, ...args: any) => {
            const params = [value, ...args]
                .map(it => JSON.stringify(it))
                .join(', ');
            return `PIPE_${pipeName}(${params})`;
        };
    }

    private getPipeName(pipe: Type<PipeTransform>) {
        return (pipe as any).__annotations__[0].name;
    }
}
