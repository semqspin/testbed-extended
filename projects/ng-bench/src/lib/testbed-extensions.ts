import { Type } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import _ from 'lodash';
import { Observable } from 'rxjs';
import { Fake } from './fake-type';

export function getFake<T>(type: Type<T>): Fake<T> {
    return TestBed.inject(type) as Fake<T>;
}

export type Emitments<T> = T[] & { latest: T };

export function watchEmitments<T>(observable: Observable<T>): Emitments<T> {
    const res: Emitments<T> = [] as any;
    addLatestGetter(res);
    observable.subscribe(data => res.push(data));
    return res;
}

function addLatestGetter(array: any[]) {
    Object.defineProperty(array, 'latest', {
        get() {
            makeSureAtLeastOneEmitment(array);
            return _.last(array);
        },
    });
}

function makeSureAtLeastOneEmitment(emittedValues: any[]) {
    if (_.isEmpty(emittedValues)) {
        throw new Error(
            'Cannot get latest emitted value since no value was emitted yet',
        );
    }
}
