/*
 * Public API Surface of ng-bench
 */
export { Host } from './lib/host';
export { NgBench } from './lib/ng-bench';
export {
    Emitments,
    getFake as fake,
    watchEmitments,
} from './lib/testbed-extensions';
