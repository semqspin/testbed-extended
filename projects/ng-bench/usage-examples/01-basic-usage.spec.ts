import { Component, OnDestroy } from '@angular/core';
import _ from 'lodash';
import { Host, NgBench } from '../src/public-api';

describe('NgBench - Basic usage', () => {
    let spy: jest.Mock<void, string[]>;
    @Component({
        selector: 'sma-selector',
        template: `
            <p>{{ title }}</p>
            <button (click)="onClick()">click me</button>
        `,
    })
    class TestedComponent implements OnDestroy {
        title = 'default-title';

        onClick() {
            this.title = 'button was clicked';
        }

        ngOnDestroy(): void {
            if (spy) {
                spy('test component was destroyed');
            }
        }
    }

    let host: Host<TestedComponent>;

    beforeEach(async () => {
        host = await NgBench.testComponent(TestedComponent).createHost();
        /**
         * TestedComponent is rendered through a host component
         *
         * The 'host' variable is a RenderResult as defined in the @testing-library/angular,
         * but with some extra methods and properties to reduce boiler plate
         * when writing tests.
         *
         * you can use
         *
         *   host.debug()
         *
         * to dump the HTML representation of the tested component during test execution:
         *
         *   <sma-selector>
         *       <p>
         *           default-title
         *       </p>
         *       <button>
         *           click me
         *       </button>
         *   </sma-selector>
         *
         */
    });

    it('host.container should be in the document', async () => {
        expect(host.container).toBeInTheDocument();
        /**
         * host.container is the DOM element for the component under test.
         *
         * Expecting host.container to be in the document is a way to insure that all dependencies
         * are properly configured and injected. It should be the first test written when testing a
         * component.
         */
    });

    it('test component can be dumped to console using host.debug()', () => {
        const logSpy = jest.spyOn(console, 'log').mockImplementation(_.noop);
        host.debug();
        expect(_.first(logSpy.mock.calls)).toMatchSnapshot();
    });

    it('host.parentContainer should be in the document', () => {
        expect(host.parentContainer).toBeInTheDocument();
        /**
         * internally, we use a host component to wrap the component under test. The host component
         * is an angular component generated automatically on the fly for the test. The template
         * of the host component contains a unique element: the component under test.
         *
         * In this example, the host template would be simply:
         *
         *   <sma-selector></sma-selector>
         *
         * The HTML representation of the host component is accessible through host.parentContainer.
         * It can be dumped with
         *
         *   host.debug(host.parentContainer)
         *
         * and looks like:
         *
         *   <div
         *       id="root1"
         *       ng-version="8.2.9"
         *   >
         *       <!--bindings={
         *       "ng-reflect-ng-if": "true"
         *   }-->
         *       <sma-selector>
         *       <p>
         *           default-title
         *       </p>
         *       <button>
         *           click me
         *       </button>
         *       </sma-selector>
         *   </div>
         *
         */
    });

    it('should render "default-title"', () => {
        expect(host.queryByText('default-title')).toBeInTheDocument();
        /**
         * to retrieve a specific HTML element, you should use one of the$ query methods provided by
         * @testing-library/angular. There are many variants and options giving sufficient flexibility
         * for most of the use cases. More info at:
         *
         * https://testing-library.com/docs/dom-testing-library/api-queries
         */
    });

    it('click on "Click me" should change the title', () => {
        host.click(host.getByText('click me'));

        /**
         * You can interact with the DOM by firing events directly from the HostRenderResult object.
         * You should always prefer these methods as they trigger fixture's detect changes automatically
         */

        expect(host.queryByText('default-title')).not.toBeInTheDocument();
        expect(host.queryByText('button was clicked')).toBeInTheDocument();
    });

    it('[DO] use host.destroyTestComponent if you expects things done in ngOnDestroy lifecycle hook', () => {
        spy = jest.fn();
        host.destroyTestComponent();
        expect(spy).lastCalledWith('test component was destroyed');
    });

    describe('bad practices', () => {
        it('[AVOID] query host.container as an HTMLElement', async () => {
            const p = host.container.querySelector('p');
            expect(p && p.textContent).toBe('default-title');

            /**
             * since it is an HTMLElement, it is possible to use host.container.querySelector and host.container.querySelectorAll
             * to verify some expectations. However, you should always prefer using RenderResult's methods and avoid
             * tying your tests to the HTML structure when possible.
             */
        });

        it('[AVOID] access host.componentInstance', async () => {
            expect(host.testInstance).toBeInstanceOf(TestedComponent);
            expect(host.testInstance.title).toBe('default-title');
            /**
             * You can access the instance of the tested component currently rendered.
             * However, you should avoid accessing the instance and prefer interacting with the component only
             * through the DOM, as a regular user would do.
             *
             * Don't invoke directly a method of the component instance. Instead, fire DOM events or through fake dependencies
             * and make expectations either on the DOM element itself or expect that a fake dependency method has been called.
             *
             * A rule of thumb should be:
             *   - fire events from outside the component class
             *   - make expectations on side-effects visible outside the component
             *
             * When following the rule, you can easily change the internal implementation of the component class
             * without breaking existing tests.
             */
        });

        it('[AVOID] firing events directly on DOM element', () => {
            const title = host.getByText('default-title');

            host.getByText('click me').click();
            expect(title.textContent).not.toBe('button was clicked');
            host.detectChanges();
            expect(title.textContent).toBe('button was clicked');

            /**
             * When firing events directly on HTML element, detectChanges is not automatically triggered and you
             * will have to trigger it manually to verify side effects of the events.
             * Most events you can fire on HTML elements are wrapped in host to trigger automatically detectChanges.
             * You should prefer:
             *
             *   host.click(host.queryByText('click me'));
             *   expect(title.textContent).toBe('button was clicked');
             *
             * However, in some case, you will have to trigger 'detectChanges' manually, for instance when firing
             * events through dependent observables. To do that, invoke
             *
             *   host.detectChanges();
             *
             * which triggers detectChanges on the host fixture.
             *
             * If your test triggers asynchronous activity, you may need to wait for fixture to be stable before
             * proceeding. For that, invoke:
             *
             *   async host.whenStable();
             */
        });
    });
});
