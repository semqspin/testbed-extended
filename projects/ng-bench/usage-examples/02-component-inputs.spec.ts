import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgBench } from '../src/public-api';

describe('NgBench - Component inputs', () => {
    describe('patching inputs', () => {
        @Component({
            selector: 'sma-selector',
            template: '<div>Hello {{ firstName }}</div>',
        })
        class TestedComponent {
            @Input() firstName = '';
        }

        it('should update DOM automatically', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs('firstName')
                .createHost();

            host.patchInputs({ firstName: 'Sergio' });
            expect(host.queryByText('Hello Sergio')).toBeInTheDocument();

            host.patchInputs({ firstName: 'Isabelle' });
            expect(host.queryByText('Hello Isabelle')).toBeInTheDocument();

            /**
             * host.patchInputs allow to modify the value of one or several inputs of the
             * tested component. Make sure to use patchInput as it does not modify directly
             * the tested component instance. Instead, it modifies the host properties which
             * are binded to the tested component. By using patchInput, you make sure that
             * all angular lifecycle hooks like ngOnChanges are triggered. Moreover,
             * patchInput triggers fixture detectChanges so that the DOM is automatically
             * updated when you invoke it.
             */
        });

        it('should throw if input name is not registered', async () => {
            const host = await NgBench.testComponent(
                TestedComponent,
            ).createHost();

            expect(() =>
                host.patchInputs({
                    firstName: 'Sergio',
                }),
            ).toThrow(
                [
                    'Following keys are not registered as component inputs: firstName.',
                    'Did you forget to register some bindings for tested element?',
                ].join('\n'),
            );

            /**
             * for patchInputs to be possible, the input names must be registered with method
             *
             *   .withInputs('firstName')
             *
             * when you forget to register one or several inputs, an exception is thrown
             * with the list of unregistered input names
             */
        });
    });

    describe('ngOnChanges', () => {
        let onChangesSpy: jest.Mock;

        @Component({
            selector: 'sma-selector',
            template: 'whatever',
        })
        class TestedComponent implements OnChanges {
            @Input() firstName = '';

            ngOnChanges(changes: SimpleChanges): void {
                if (onChangesSpy) {
                    onChangesSpy(changes);
                }
            }
        }

        it('automatically triggered when component is rendered', async () => {
            onChangesSpy = jest.fn();

            await NgBench.testComponent(TestedComponent)
                .bindInputs('firstName')
                .createHost();

            expect(onChangesSpy).toBeCalledTimes(1);
            expect(onChangesSpy).lastCalledWith({
                firstName: {
                    currentValue: undefined,
                    firstChange: true,
                    previousValue: undefined,
                },
            });

            /**
             * When creating component directly with TestBed.createComponent(TestedComponent),
             * the angular hooks like ngOnChanges are not triggered when you modify inputs directly
             * on the tested component instance. This  makes tests hard to write since you must
             * be aware that the hooks exist and remember to call them manually.
             *
             * Because our testing library is using a host component to wrap the component under test,
             * we gain these hooks to be called automatically.
             *
             * Under the hood, patchInputs modifies HOST members which are bound to the
             * tested component inputs, so that the tested component inputs are not modified directly
             * but through the whole angular binding mecanism.
             *
             * You can picture the generated host class to be similar to
             *
             *   @Component({
             *     template: '<sma-selector [firstName]="firstName"></sma-selector>'
             *   })
             *   class Host {
             *     firstName: string;
             *   }
             *
             * patchInputs actually modifies the value of "firstName" in the host component instance
             * and triggers detectChanges on the host fixture, which in turn will update the firstName
             * of the tested component
             */
        });

        it('automatically triggered each time inputs are patched with a different value', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs('firstName')
                .createHost();
            onChangesSpy = jest.fn();

            host.patchInputs({ firstName: 'Sergio' });
            expect(onChangesSpy).toBeCalledTimes(1);
            expect(onChangesSpy).lastCalledWith({
                firstName: {
                    currentValue: 'Sergio',
                    firstChange: false,
                    previousValue: undefined,
                },
            });

            host.patchInputs({ firstName: 'Isabelle' });
            expect(onChangesSpy).toBeCalledTimes(2);
            expect(onChangesSpy).lastCalledWith({
                firstName: {
                    currentValue: 'Isabelle',
                    firstChange: false,
                    previousValue: 'Sergio',
                },
            });
        });

        it('not triggered when patching input with same value as current one', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs('firstName')
                .createHost();

            host.patchInputs({ firstName: 'Sergio' });
            onChangesSpy = jest.fn();

            host.patchInputs({ firstName: 'Sergio' });
            expect(onChangesSpy).not.toBeCalled();
        });
    });

    describe('only registered inputs can be patched', () => {
        @Component({
            selector: 'sma-selector',
            template: 'whatever',
        })
        class TestedComponent {
            @Input() firstName = '';
            @Input() lastName = '';
            @Input() age = -1;
        }

        it('should throw when patching unregistered inputs', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs('firstName')
                .createHost();

            expect(() =>
                host.patchInputs({
                    firstName: 'Sergio',
                    lastName: 'Mazzoleni',
                    age: 46,
                }),
            ).toThrow(
                [
                    'Following keys are not registered as component inputs: age, lastName.',
                    'Did you forget to register some bindings for tested element?',
                ].join('\n'),
            );
        });
    });

    describe('only @Input decorated properties can be registered', () => {
        @Component({
            selector: 'sma-selector',
            template: 'whatever',
        })
        class TestedComponent {
            firstName = '';
            lastName = '';
        }

        it('should throw when rendering components with non-decorated inputs', async () => {
            await expect(
                NgBench.testComponent(TestedComponent)
                    .bindInputs('firstName', 'lastName')
                    .createHost(),
            ).rejects.toThrow(
                [
                    'Invalid bindings for component "TestedComponent"',
                    '  - "firstName" has no @Input() decorator',
                    '  - "lastName" has no @Input() decorator',
                ].join('\n'),
            );
        });
    });

    describe('default values of inputs', () => {
        @Component({
            selector: 'sma-whatever',
            template: 'whatever',
        })
        class TestedComponent {
            @Input() color = 'the-default-color';
        }

        it('when not bound, input property has value from class instantiation', async () => {
            const host = await NgBench.testComponent(
                TestedComponent,
            ).createHost();
            expect(host.testInstance.color).toBe('the-default-color');
        });

        it('bound input has undefined value by default', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs('color')
                .createHost();
            expect(host.testInstance.color).toBeUndefined();
        });

        it('use an object to bind a specific value before component is created', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs({ color: 'a-custom-color' })
                .createHost();
            expect(host.testInstance.color).toBe('a-custom-color');
        });
    });
});
