import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgBench } from '../src/public-api';

describe('NgBench - Component outputs', () => {
    describe('get output event values emitted by component', () => {
        @Component({
            selector: 'sma-selector',
            template: '<button (click)="onClick()">click me</button>',
        })
        class TestedComponent {
            @Output() message = new EventEmitter<string>();
            clickCounter = new EventEmitter<number>();

            index = 0;
            onClick() {
                const events = ['hello', 'world', 'foo', 'bar'];
                this.message.emit(events[this.index]);
                this.index++;
                this.clickCounter.emit(this.index);
            }
        }

        it('[DO] bind output property when building host and use host.getOutputEvents to retrieve values emitted so far', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindOutputs('message')
                .createHost();

            host.click(host.getByText('click me'));
            host.click(host.getByText('click me'));
            host.click(host.getByText('click me'));

            const messages = host.getOutputEvents('message');
            expect(messages).toEqual(['hello', 'world', 'foo']);
        });

        it('[NOTE] returned array will also contain events emitted after assignment', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindOutputs('message')
                .createHost();

            const messages = host.getOutputEvents('message');
            expect(messages).toBeArrayOfSize(0);

            host.click(host.getByText('click me'));
            expect(messages).toEqual(['hello']);
        });

        it('[TIP] use latestOutputEvent instead of getOutputEvents as shortcut to latest element', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindOutputs('message')
                .createHost();

            host.click(host.getByText('click me'));
            expect(host.latestOutputEvent('message')).toBe('hello');

            host.click(host.getByText('click me'));
            expect(host.latestOutputEvent('message')).toBe('world');
        });

        it('latestOutputEvent should throw if no events emitted so far', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindOutputs('message')
                .createHost();

            expect(() => host.latestOutputEvent('message')).toThrow(
                'Output event "message" was never emitted.',
            );
        });

        it('[DO NOT] getOutputEvents without binding output when building host throws error', async () => {
            const host = await NgBench.testComponent(
                TestedComponent,
            ).createHost();

            expect(() => host.getOutputEvents('message')).toThrow(
                [
                    `Key "message" is not registered as component output`,
                    'Did you forget to register some bindings for tested element?',
                ].join('\n'),
            );
        });
    });

    describe('only @Output decorated properties can be bound as output while building host', () => {
        @Component({
            selector: 'whatever',
            template: 'whatever',
        })
        class TestedComponent {
            @Output() event1 = new EventEmitter<number>();
            event2 = new EventEmitter<number>();
            event3 = new EventEmitter<number>();
        }

        it('should throw when rendering components with non-decorated outputs', async () => {
            await expect(
                NgBench.testComponent(TestedComponent)
                    .bindOutputs('event3', 'event2', 'event1')
                    .createHost(),
            ).rejects.toThrow(
                [
                    'Invalid bindings for component "TestedComponent"',
                    '  - "event2" has no @Output() decorator',
                    '  - "event3" has no @Output() decorator',
                ].join('\n'),
            );
        });
    });

    describe('output events emitted during component initialization', () => {
        @Component({
            selector: 'some-selector',
            template: 'whatever-the-template',
        })
        class TestedComponent implements OnInit {
            @Output() message = new EventEmitter<string>();

            ngOnInit() {
                this.message.emit('ngOnInit invoked');
            }
        }

        it('should be part of the array returned by host.getOutputEvents', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindOutputs('message')
                .createHost();

            expect(host.getOutputEvents('message')).toEqual([
                'ngOnInit invoked',
            ]);
        });
    });
});
