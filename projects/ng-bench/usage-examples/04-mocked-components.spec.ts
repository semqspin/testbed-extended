import { Component, EventEmitter, Input, Output } from '@angular/core';
import _ from 'lodash';
import { NgBench } from '../src/public-api';

describe('NgBench - mocked component(s) inside tested compnonent', () => {
    describe('basics', () => {
        @Component({
            selector: 'sma-sub',
            template: 'whatever',
        })
        class SubComponent {}

        @Component({
            selector: 'sma-selector',
            template: '<sma-sub></sma-sub>',
        })
        class TestedComponent {}

        it('building host should console.error if subcomponent class is not declared', async () => {
            // NOTE: in previous versions of TestBed, an exception was thrown, but not anymore
            expect.assertions(1);
            jest.spyOn(console, 'error').mockImplementation(errorMessage => {
                expect(errorMessage).toEqual(
                    [
                        "NG0304: 'sma-sub' is not a known element:",
                        "1. If 'sma-sub' is an Angular component, then verify that it is part of this module.",
                        "2. If 'sma-sub' is a Web Component then add 'CUSTOM_ELEMENTS_SCHEMA' to the '@NgModule.schemas' of this component to suppress this message.",
                    ].join('\n'),
                );
            });
            await NgBench.testComponent(TestedComponent).createHost();
        });

        it('sub component instance can be retrieved with host.queryInstanceByDirective', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent)
                .createHost();

            expect(host.queryInstanceByDirective(SubComponent)).not.toBeNil();
        });

        it('sub component HTML element can be retrieved with host.queryByDirective', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent)
                .createHost();

            expect(host.queryByDirective(SubComponent)).toBeInTheDocument();
            expect(host.queryByDirective(SubComponent)).toMatchSnapshot();
        });

        it('for consistency with @testing-library, there is a host.getByDirective', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent)
                .createHost();

            expect(host.getByDirective(SubComponent)).toBeInTheDocument();
        });

        it('queryInstanceByDirective should return null if sub component instance can not be found on current host', async () => {
            @Component({
                selector: 'unused',
                template: 'whatever',
            })
            class UnusedComponent {}

            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent, UnusedComponent)
                .createHost();

            expect(host.queryInstanceByDirective(UnusedComponent)).toBeNull();
        });

        it('queryByDirective should return null if sub component element can not be found on current host', async () => {
            @Component({
                selector: 'unused',
                template: 'whatever',
            })
            class UnusedComponent {}

            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent, UnusedComponent)
                .createHost();

            expect(host.queryByDirective(UnusedComponent)).toBeNull();
        });

        it('getInstanceByDirective should throw if sub component instance can not be found on current host', async () => {
            @Component({
                selector: 'unused',
                template: 'whatever',
            })
            class UnusedComponent {}

            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent, UnusedComponent)
                .createHost();

            expect(() => host.getInstanceByDirective(UnusedComponent)).toThrow(
                'Expected unique instance for directive of type "UnusedComponent" but found 0 instance(s)',
            );
        });

        it('getByDirective should throw if sub component element can not be found on current host', async () => {
            @Component({
                selector: 'unused',
                template: 'whatever',
            })
            class UnusedComponent {}

            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent, UnusedComponent)
                .createHost();

            expect(() => host.getByDirective(UnusedComponent)).toThrow(
                'Expected unique HTML element for directive of type "UnusedComponent" but found 0 HTML element(s)',
            );
        });
    });

    describe('inputs and outputs on sub component', () => {
        let spy: jest.Mock;

        @Component({
            selector: 'sma-sub',
            template: 'whatever',
        })
        class SubComponent {
            @Input() data = -1;
            @Output() someEvent = new EventEmitter<string>();
        }

        @Component({
            selector: 'whatever',
            template:
                '<sma-sub [data]="1234" (someEvent)="onEvent($event)"></sma-sub>',
        })
        class TestedComponent {
            onEvent(payload: string) {
                if (spy) {
                    spy(payload);
                }
            }
        }

        it('sub component instance should have the tested component inputs', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent)
                .createHost();

            expect(host.getInstanceByDirective(SubComponent).data).toBe(1234);
        });

        it('sub component events can be simulated', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent)
                .createHost();

            spy = jest.fn();

            host.getInstanceByDirective(SubComponent).someEvent.emit('hello');
            expect(spy).toBeCalledTimes(1);
            expect(spy).lastCalledWith('hello');
        });
    });

    describe('trigger detect changes on host automatically when mocked directive or component output emits', () => {
        @Component({
            selector: 'sma-sub',
            template: 'whatever',
        })
        class SubComponent {
            @Output() message = new EventEmitter<string>();
        }

        @Component({
            selector: 'whatever',
            template: `
                <sma-sub (message)="onMessage($event)"></sma-sub>
                <div>Latest message: {{ latestMessage }}</div>
            `,
        })
        class TestedComponent {
            latestMessage = '';
            onMessage(event: string) {
                this.latestMessage = event;
            }
        }

        it('should ', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(SubComponent)
                .createHost();

            host.getInstanceByDirective(SubComponent).message.emit(
                'Hello world',
            );
            expect(
                host.getByText('Latest message: Hello world'),
            ).toBeInTheDocument();
        });
    });

    describe('several subcomponents of same type', () => {
        let spy: jest.Mock;

        @Component({ selector: 'sma-item', template: 'whatever' })
        class ItemComponent {
            @Input() data = '';
            @Output() click = new EventEmitter<number>();
        }

        @Component({
            selector: 'sma-selector',
            template: `
                <sma-item
                    *ngFor="let item of items"
                    [data]="item.data"
                    (click)="onClick(item.id)"
                ></sma-item>
            `,
        })
        class TestedComponent {
            items = [
                { id: 123, data: 'one' },
                { id: 456, data: 'two' },
                { id: 789, data: 'three' },
            ];

            onClick(id: number) {
                spy(id);
            }
        }

        it('sub component instances can be retrieved with host.getAllSubComponents', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(ItemComponent)
                .createHost();

            const subComponents = host.getAllInstancesByDirective(
                ItemComponent,
            );

            expect(subComponents).toBeArrayOfSize(3);
            expect(_.map(subComponents, 'data')).toEqual([
                'one',
                'two',
                'three',
            ]);

            spy = jest.fn();
            subComponents[1].click.emit();
            expect(spy).lastCalledWith(456);
        });

        it('host.queryInstanceByDirective should throw when there are more than one sub component', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(ItemComponent)
                .createHost();

            expect(() => host.queryInstanceByDirective(ItemComponent)).toThrow(
                `Found multiple directives of type "ItemComponent". If this is intentional, use the 'getAllInstancesByDirective' variant`,
            );
        });

        it('host.getInstanceByDirective should throw when there are more than one sub component', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(ItemComponent)
                .createHost();

            expect(() => host.getInstanceByDirective(ItemComponent)).toThrow(
                `Expected unique instance for directive of type "ItemComponent" but found 3 instance(s)`,
            );
        });

        it('host.queryByDirective should throw when there are more than one sub component', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(ItemComponent)
                .createHost();

            expect(() => host.queryByDirective(ItemComponent)).toThrow(
                `Found multiple directives of type "ItemComponent". If this is intentional, use the 'getAllByDirective' variant`,
            );
        });

        it('host.getByDirective should throw when there are more than one sub component', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockComponents(ItemComponent)
                .createHost();

            expect(() => host.getByDirective(ItemComponent)).toThrow(
                `Expected unique HTML element for directive of type "ItemComponent" but found 3 HTML element(s)`,
            );
        });
    });
});
