import { Component, Injectable, Pipe, PipeTransform } from '@angular/core';
import { NgBench } from '../src/public-api';

describe('Mocked pipe used in template', () => {
    describe('mockPipe', () => {
        @Pipe({ name: 'translate' })
        class TranslatePipe implements PipeTransform {
            transform(_value: any, ..._args: any[]): number {
                throw new Error('not implemented');
            }
        }

        it('should replace pipe call in templates by PIPE_pipe_name(main_argument)]', async () => {
            @Component({
                selector: 'whatever',
                template: ` <div>{{ 'test' | translate }}</div> `,
            })
            class TestedComponent {}

            const host = await NgBench.testComponent(TestedComponent)
                .mockPipe(TranslatePipe)
                .createHost();

            expect(
                host.queryByText('PIPE_translate("test")', {
                    exact: false,
                }),
            ).toBeInTheDocument();
        });

        it('should replace pipe call in templates by PIPE_pipe_name(main_argument, ...optional arguments)', async () => {
            @Component({
                selector: 'whatever',
                template: `
                    <div>{{ 'test' | translate: 123:'hello':'world' }}</div>
                `,
            })
            class TestedComponent {}

            const host = await NgBench.testComponent(TestedComponent)
                .mockPipe(TranslatePipe)
                .createHost();

            expect(
                host.queryByText(
                    'PIPE_translate("test", 123, "hello", "world")',
                    {
                        exact: false,
                    },
                ),
            ).toBeInTheDocument();
        });

        it('pipe can be mocked by name instead of pipe', async () => {
            @Component({
                selector: 'whatever',
                template: ` <div>{{ 'test' | capitalize }}</div> `,
            })
            class TestedComponent {}

            const host = await NgBench.testComponent(TestedComponent)
                .mockPipe('capitalize')
                .createHost();

            expect(
                host.queryByText('PIPE_capitalize("test")', {
                    exact: false,
                }),
            ).toBeInTheDocument();
        });

        it('pipe can be mocked with a custom transform method', async () => {
            @Component({
                selector: 'whatever',
                template: ` <div>{{ 'test' | translate: 12:'abc' }}</div> `,
            })
            class TestedComponent {}

            const pipeTransform = jest
                .fn()
                .mockReturnValue('MY_FAKE_IMPLEMENTATION');

            const host = await NgBench.testComponent(TestedComponent)
                .mockPipe(TranslatePipe, pipeTransform)
                .createHost();

            expect(
                host.queryByText('MY_FAKE_IMPLEMENTATION', {
                    exact: false,
                }),
            ).toBeInTheDocument();
        });

        it('should provide the mocked pipe, so that it can be injected', () => {
            @Pipe({ name: 'somePipe' })
            class SomePipe implements PipeTransform {
                transform(_value: any, ..._args: any[]): number {
                    throw new Error('not implemented');
                }
            }

            @Injectable()
            class SomeService {
                constructor(private somePipe: SomePipe) {}

                methodInvokingSomePipeTransform() {
                    return this.somePipe.transform('one', 'two', 'three');
                }
            }

            const myService = NgBench.testInjectable(SomeService)
                .mockPipe(SomePipe)
                .createInstance();

            expect(myService.methodInvokingSomePipeTransform()).toEqual(
                'PIPE_somePipe("one", "two", "three")',
            );
        });
    });

    describe('mockPipes', () => {
        @Pipe({ name: 'translate' })
        class TranslatePipe implements PipeTransform {
            transform(_value: any, ..._args: any[]): number {
                throw new Error('not implemented');
            }
        }

        it('should mock each pipe with default transform', () => {
            class TestedComponent {}

            const testBuilder = NgBench.testComponent(TestedComponent);
            const spy = jest.spyOn(testBuilder, 'mockPipe');

            testBuilder.mockPipes(TranslatePipe, 'capitalize');

            expect(spy).toBeCalledWith(TranslatePipe);
            expect(spy).toBeCalledWith('capitalize');
        });
    });
});
