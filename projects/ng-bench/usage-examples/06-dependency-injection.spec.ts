import { Component, Injectable, OnInit } from '@angular/core';
import { EMPTY, Observable, of, Subject } from 'rxjs';
import { fake, NgBench } from '../src/public-api';

describe('NgBench - dependency injection', () => {
    describe('injecting fake service into test a component', () => {
        class MyService {}

        @Component({ selector: 'sma-whatever', template: 'whatever' })
        class TestedComponent {
            constructor(_myService: MyService) {}
        }

        it('should throw if MyService is not provided', async () => {
            expect.assertions(1);
            try {
                await NgBench.testComponent(TestedComponent).createHost();
            } catch (error: any) {
                expect(error.toString()).toInclude(
                    'NullInjectorError: No provider for MyService!',
                );
            }
        });

        it('provide a fake MyService before rendering component to avoid injection error', async () => {
            await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService)
                .createHost();
        });

        it('fake service can be retrieved with function "fake" and is empty object', async () => {
            await NgBench.testComponent(TestedComponent)
                .mockProviders(MyService)
                .createHost();

            expect(fake(MyService)).toEqual({});
        });
    });

    describe('Caution: injectable class can be given a "providedIn" property', () => {
        let spy: jest.Mock;

        @Injectable({ providedIn: 'root' })
        class SomeInjectedService {
            someMethod() {
                spy('real implementation was invoked');
            }
        }

        @Injectable()
        class TestedClass {
            constructor(_service: SomeInjectedService) {}
        }

        beforeEach(() => {
            spy = jest.fn();
        });

        it('should inject the real dependency if no specific provider is given to TestBed', () => {
            expect(() =>
                NgBench.testInjectable(TestedClass).createInstance(),
            ).not.toThrow();

            fake(SomeInjectedService).someMethod();
            expect(spy).lastCalledWith('real implementation was invoked');
        });

        it('[DO] provide a fake dependency to get full control over it and ignore the "real" dependency', () => {
            NgBench.testInjectable(
                TestedClass,
            ).mockProvider(SomeInjectedService, { someMethod: jest.fn() });
            fake(SomeInjectedService).someMethod();
            expect(spy).not.toBeCalled();
        });

        it('[DO NOT] not mocking method of the service invoked during test should throw an exception', () => {
            NgBench.testInjectable(TestedClass).mockProvider(
                SomeInjectedService,
            );

            expect(() => fake(SomeInjectedService).someMethod()).toThrow(
                '.someMethod is not a function',
            );

            fake(SomeInjectedService).someMethod = jest.fn();
            expect(() => fake(SomeInjectedService).someMethod()).not.toThrow();
        });
    });

    describe('expectations on service method called', () => {
        class MyService {
            saveData(_data: string) {
                // we are not interested in real implementation
                throw new Error('not implemented');
            }
        }

        @Component({
            selector: 'sma-whatever',
            template: '<button (click)="save()">save</button>',
        })
        class TestedComponent {
            constructor(private myService: MyService) {}
            save() {
                this.myService.saveData('hello-world');
            }
        }

        it('[DO] assign a jest.Mock to the service method before calling it', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockProviders(MyService)
                .createHost();

            fake(MyService).saveData = jest.fn();
            host.click(host.getByText('save'));
            expect(fake(MyService).saveData).toBeCalledTimes(1);
            expect(fake(MyService).saveData).lastCalledWith('hello-world');
        });
    });

    describe('expectations depending on service method returned value', () => {
        class MyService {
            loadData(): Observable<string> {
                throw new Error('not implemented');
            }
        }

        @Component({
            selector: 'sma-whatever',
            template: `
                <button (click)="load()">load</button>
                <div>{{ data }}</div>
            `,
        })
        class TestedComponent {
            data = '';
            constructor(private myService: MyService) {}
            load() {
                this.myService.loadData().subscribe(data => (this.data = data));
            }
        }

        it('should render service returned value in the document', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService)
                .createHost();

            fake(MyService).loadData = jest.fn(() => of('fake-data'));
            host.click(host.getByText('load'));
            expect(host.queryByText('fake-data')).toBeInTheDocument();
        });

        it('[TIP] consider initializing the fake provider when building host if it makes test more readable ', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService, {
                    loadData: jest.fn(() => of('fake-data')),
                })
                .createHost();

            host.click(host.getByText('load'));
            expect(host.queryByText('fake-data')).toBeInTheDocument();
        });
    });

    describe('calling service methods as soon as component is rendered', () => {
        class MyService {
            loadData(): Observable<string> {
                throw new Error('not implemented');
            }
        }

        @Component({
            selector: 'sma-whatever',
            template: '<div>{{ data }}</div>',
        })
        class TestedComponent implements OnInit {
            data = '';

            constructor(private myService: MyService) {}

            ngOnInit(): void {
                this.myService.loadData().subscribe(data => (this.data = data));
            }
        }

        it('createHost should throw because we did not get a chance to mock service method', async () => {
            expect.assertions(2);
            try {
                await NgBench.testComponent(TestedComponent)
                    .mockProvider(MyService)
                    .createHost();
            } catch (error: any) {
                expect(error.name).toBe('TypeError');
                expect(error.message).toBe(
                    'this.myService.loadData is not a function',
                );
            }
        });

        it('[DO] initialize fake provider with fake instance when building host', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService, {
                    loadData: jest.fn(() => of('fake-data')),
                })
                .createHost();

            expect(host.queryByText('fake-data')).toBeInTheDocument();
        });

        it('[DO] setup the fake provider with beforeRender hook', async () => {
            /**
             * this variant may be helpful if we need to share logic between
             * several fake providers
             */
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService)
                .beforeRender(() => {
                    fake(MyService).loadData = jest.fn(() => of('fake-data'));
                })
                .createHost();

            expect(host.queryByText('fake-data')).toBeInTheDocument();
        });

        it('[DO] use noRenderingAtHostCreation and host.renderTestComponent', async () => {
            /**
             * this variant may be helpful if we need to test several setups:
             * put the host creation (without tested component rendering) in a 'beforeEach',
             * and place the setup, rendering and expectations in a 'it' function
             */
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService)
                .noRenderingAtHostCreation()
                .createHost();

            fake(MyService).loadData = jest.fn(() => of('fake-data'));
            host.renderTestComponent();

            expect(host.queryByText('fake-data')).toBeInTheDocument();
        });

        it('[DO] reset the test component and pass a setup function', async () => {
            /**
             * this variant may be helpful if we need to test several setups,
             * but be aware of the possible side effects of the first rendering
             */
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService, {
                    loadData: jest.fn().mockReturnValue(EMPTY),
                })
                .createHost();

            await host.resetTestComponent(() => {
                fake(MyService).loadData = jest.fn(() => of('fake-data'));
            });
            expect(host.queryByText('fake-data')).toBeInTheDocument();
        });

        it('[DO] setup again and reset', async () => {
            /**
             * this variant may be helpful if we need to test several setups,
             * but be aware of the possible side effects of the first rendering
             */
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService, {
                    loadData: jest.fn().mockReturnValue(EMPTY),
                })
                .createHost();

            fake(MyService).loadData = jest.fn(() => of('fake-data'));
            await host.resetTestComponent();
            expect(host.queryByText('fake-data')).toBeInTheDocument();
        });
    });

    describe('providing several fake providers', () => {
        class SomeService {
            doSomething() {
                throw new Error('not implemented');
            }
        }

        class SomeOtherService {
            doSomethingElse() {
                throw new Error('not implemented');
            }
        }

        @Component({
            selector: 'whatever',
            template: 'whatever',
        })
        class TestedComponent {
            constructor(
                // @ts-ignore
                private service: SomeService,
                // @ts-ignore
                private otherService: SomeOtherService,
            ) {}
        }

        it('call mockProvider for each type to be mocked (order is not relevant)', async () => {
            await NgBench.testComponent(TestedComponent)
                .mockProvider(SomeService)
                .mockProvider(SomeOtherService)
                .createHost();
        });

        it('[TIP] if no initialization is required, you can use the plural form', async () => {
            await NgBench.testComponent(TestedComponent)
                .mockProviders(SomeService, SomeOtherService)
                .createHost();
        });
    });

    describe('Automatic detectChanges', () => {
        class MyService {
            firstName = '';
            ageChanged$: Observable<number> = EMPTY;
            // @ts-ignore
            getMessageById$(id: number): Observable<string> {
                throw new Error('not implemented');
            }
        }

        @Component({ selector: 'sma-whatever', template: 'whatever' })
        class TestedComponent {
            // @ts-ignore
            constructor(private myService: MyService) {}
        }

        it('when fake service property is set', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService)
                .createHost();

            const spy = jest.spyOn(host, 'detectChanges');
            fake(MyService).firstName = 'hello';
            expect(spy).toBeCalled();
        });

        it('when fake service subject emits', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService)
                .createHost();

            fake(MyService).ageChanged$ = new Subject();

            const spy = jest.spyOn(host, 'detectChanges');
            fake(MyService).ageChanged$.next(34);
            fake(MyService).ageChanged$.next(35);
            expect(spy).toBeCalledTimes(2);
        });

        it('when observable returned by fake service method emits', async () => {
            const message$ = new Subject<string>();
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService, {
                    getMessageById$: jest.fn().mockReturnValue(message$),
                })
                .createHost();

            fake(MyService).getMessageById$(123);

            const spy = jest.spyOn(host, 'detectChanges');
            message$.next('first message');
            message$.next('second message');
            expect(spy).toBeCalledTimes(2);
        });

        it('[WARNING] the fake service method must be called at least once for the detectChanges to be triggered', async () => {
            const message$ = new Subject<string>();
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService, {
                    getMessageById$: jest.fn().mockReturnValue(message$),
                })
                .createHost();

            const spy = jest.spyOn(host, 'detectChanges');
            message$.next('first message');
            expect(spy).not.toBeCalled();
        });

        it('[DO] as long as the component under test calls the method, this is not an issue', async () => {
            @Component({ selector: 'sma-whatever', template: 'whatever' })
            class TestedComponentConsumingFakeMethod implements OnInit {
                constructor(private myService: MyService) {}
                ngOnInit(): void {
                    this.myService.getMessageById$(1234);
                }
            }
            const message$ = new Subject<string>();
            const host = await NgBench.testComponent(
                TestedComponentConsumingFakeMethod,
            )
                .mockProvider(MyService, {
                    getMessageById$: jest.fn().mockReturnValue(message$),
                })
                .createHost();

            const spy = jest.spyOn(host, 'detectChanges');
            message$.next('first message');
            expect(spy).toBeCalled();
        });

        it('detectChanges should be triggered only once if subject is used several times', async () => {
            const message$ = new Subject<string>();
            const host = await NgBench.testComponent(TestedComponent)
                .mockProvider(MyService, {
                    getMessageById$: jest.fn().mockReturnValue(message$),
                })
                .createHost();

            fake(MyService).getMessageById$(123);
            fake(MyService).getMessageById$(123);
            fake(MyService).getMessageById$(123);

            const spy = jest.spyOn(host, 'detectChanges');
            message$.next('first message');
            expect(spy).toBeCalledTimes(1);
        });
    });
});
