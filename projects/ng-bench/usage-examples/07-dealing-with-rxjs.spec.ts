import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { fake, NgBench, watchEmitments } from '../src/public-api';

describe('NgBench - dealing with RxJs (observables)', () => {
    describe('obsevable property of dependency used in tested class constructor', () => {
        let spy: jest.Mock;
        class SomeInjectedService {
            get message$(): Observable<string> {
                throw new Error('not implemented');
            }
        }

        @Injectable()
        class TestedClass {
            constructor(service: SomeInjectedService) {
                service.message$.subscribe(message => {
                    spy?.(message);
                });
            }
        }

        it('[DO NOT] get TestedClass before mocking the observable property', () => {
            const testBuilder = NgBench.testInjectable(
                TestedClass,
            ).mockProvider(SomeInjectedService);
            expect(() => testBuilder.createInstance()).toThrow(
                "Cannot read property 'subscribe' of undefined",
            );
        });

        it('[DO] set the observable property to a Subject so that you can control observable emitments', () => {
            NgBench.testInjectable(TestedClass)
                .mockProvider(SomeInjectedService, { message$: new Subject() })
                .createInstance();

            spy = jest.fn();
            fake(SomeInjectedService).message$.next('hello');
            expect(spy).lastCalledWith('hello');
            /**
             * Notice that the fake service has type Fake<SomeInjectedService> which have the
             * same message$ property as original SomeInjectedService class but with type
             * Subject<string> instead of Observable<string>. Hence, when accessing
             * fake(SomeInjectedService).message$, we can use Subject.next method, which is visible
             * in intellisense suggestions and is strongly typed, meaning that
             *
             *   fake(SomeInjectedService).message$.next(1234);
             *
             * would show following error directly in editor:
             *
             *   Argument of type '1234' is not assignable to parameter of type 'string'.ts(2345)
             */
        });

        it('[DO] variant: set the observable property to a BehaviorSubject with an initial value', () => {
            spy = jest.fn();
            NgBench.testInjectable(TestedClass)
                .mockProvider(SomeInjectedService, {
                    message$: new BehaviorSubject('initial-value'),
                })
                .createInstance();

            expect(spy).lastCalledWith('initial-value');

            fake(SomeInjectedService).message$.next('hello');
            expect(spy).lastCalledWith('hello');
        });
    });

    describe('watch values emitted by an observable exposed in a service', () => {
        @Injectable()
        class SomeService {
            private valueSubject = new Subject<string>();
            get value$() {
                return this.valueSubject.asObservable();
            }

            emitValues(...values: string[]) {
                values.forEach(value => this.valueSubject.next(value));
            }
        }

        it('[TIP] use watchEmitments helper function to get an array of emitted values', () => {
            const service = NgBench.testInjectable(
                SomeService,
            ).createInstance();

            const emittedValues = watchEmitments(service.value$);
            expect(emittedValues).toHaveLength(0);

            service.emitValues('one', 'two');
            expect(emittedValues).toHaveLength(2);
            expect(emittedValues).toEqual(['one', 'two']);

            service.emitValues('three', 'four', 'five');
            expect(emittedValues).toHaveLength(5);
            expect(emittedValues).toEqual([
                'one',
                'two',
                'three',
                'four',
                'five',
            ]);
        });

        it('[TIP] array returned by watchEmitments has a getter that returns the latest emitted value', () => {
            const service = NgBench.testInjectable(
                SomeService,
            ).createInstance();

            const emittedValues = watchEmitments(service.value$);
            service.emitValues('one', 'two');
            expect(emittedValues.latest).toEqual('two');

            service.emitValues('three', 'four', 'five');
            expect(emittedValues.latest).toEqual('five');
        });

        it('latest getter throws when no value emitted yet', () => {
            const service = NgBench.testInjectable(
                SomeService,
            ).createInstance();

            const emittedValues = watchEmitments(service.value$);
            expect(() => emittedValues.latest).toThrow(
                'Cannot get latest emitted value since no value was emitted yet',
            );
        });
    });
});
