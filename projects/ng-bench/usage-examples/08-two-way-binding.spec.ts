import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgBench } from '../src/public-api';

describe('NgBench - Two-way binding', () => {
    @Component({
        selector: 'sma-data',
        template: ` <button (click)="onClick()">add !</button> `,
    })
    class TestedComponent {
        @Input() data = '';
        // NOTE: must have 'Change' suffix for [(data)] to work
        @Output() dataChange = new EventEmitter<string>();

        onClick() {
            this.data = this.data + '!';
            this.dataChange.emit(this.data);
        }
    }

    describe('reminder: banana in a box [()] syntax', () => {
        @Component({
            selector: 'sma-host',
            template: ` <sma-data [(data)]="firstName"></sma-data> `,
        })
        class HostComponent {
            firstName = '';
        }

        /**
         * Remark: [(data)]="firstName" is just a syntaxic sugar for
         *
         *   [data]="firstName" (dataChange)="firstName = $event"
         */

        it('should update value in both direction', async () => {
            const host = await NgBench.testComponent(HostComponent)
                .declare(TestedComponent)
                .createHost();

            const subComponent = host.getInstanceByDirective(TestedComponent);

            // modify input
            host.testInstance.firstName = 'Sergio';
            host.detectChanges();
            expect(subComponent.data).toBe('Sergio');

            // input modified from inside tested component
            host.click(host.getByText('add !'));
            expect(host.testInstance.firstName).toBe('Sergio!');
        });
    });

    describe('TestBed extended way to test 2 way data binding', () => {
        it('[DO] declare the input property, its output counterpart (xxxChange) and set flag twoWayDataBinding', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs('data')
                .bindOutputs('dataChange')
                .enableTwoWayDataBinding()
                .createHost();

            host.patchInputs({ data: 'Sergio' });
            expect(host.testInstance.data).toBe('Sergio');

            host.click(host.getByText('add !'));
            expect(host.hostInstance.data).toBe('Sergio!');
        });

        it('[NOTE] output events are still accessible', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .bindInputs('data')
                .bindOutputs('dataChange')
                .enableTwoWayDataBinding()
                .createHost();

            host.patchInputs({ data: 'Sergio' });
            host.click(host.getByText('add !'));

            expect(host.latestOutputEvent('dataChange')).toBe('Sergio!');
        });
    });
});
