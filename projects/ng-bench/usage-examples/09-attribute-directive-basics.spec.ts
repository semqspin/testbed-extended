import {
    Component,
    Directive,
    ElementRef,
    HostListener,
    Input,
} from '@angular/core';
import { NgBench } from '../src/public-api';

describe('NgBench - Attribute directive basics', () => {
    @Directive({
        selector: '[smaHighlight]', // notice the brackets
    })
    class HighlightDirective {
        @Input('smaHighlight') highlightColor = ''; // tslint:disable-line: no-input-rename
        constructor(private el: ElementRef) {}

        @HostListener('mouseenter') onMouseEnter() {
            this.highlight(this.highlightColor);
        }

        @HostListener('mouseleave') onMouseLeave() {
            this.highlight('');
        }

        private highlight(color: string) {
            this.el.nativeElement.style.backgroundColor = color;
        }
    }

    describe('[DO] use createHostFor the same way as for components', () => {
        it('should alter host element on which the directive is applied', async () => {
            const host = await NgBench.testComponent(HighlightDirective)
                .bindInputs('highlightColor')
                .createHost();

            host.patchInputs({ highlightColor: 'green' });

            expect(host.container.style.backgroundColor).toBe('');

            host.mouseEnter(host.container);
            expect(host.container.style.backgroundColor).toBe('green');

            host.mouseLeave(host.container);
            expect(host.container.style.backgroundColor).toBe('');
        });
    });

    describe('[avoid] use a host component, since it is exactly what prepareHostFor will do for you', () => {
        @Component({
            selector: 'sma-whatever',
            template: '<div [smaHighlight]="color">Hello world</div>',
        })
        class HostComponent {
            @Input() color = '';
        }

        it('should alter host element on which the directive is applied', async () => {
            const host = await NgBench.testComponent(HostComponent)
                .bindInputs('color')
                .declare(HighlightDirective)
                .createHost();

            host.patchInputs({ color: 'red' });

            const highlightedDiv = host.getByText('Hello world');
            expect(highlightedDiv.style.backgroundColor).toBe('');

            host.mouseEnter(highlightedDiv);
            expect(highlightedDiv.style.backgroundColor).toBe('red');

            host.mouseLeave(highlightedDiv);
            expect(highlightedDiv.style.backgroundColor).toBe('');
        });
    });
});
