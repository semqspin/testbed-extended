import {
    Component,
    Directive,
    EventEmitter,
    HostListener,
    Input,
    Output,
} from '@angular/core';
import { NgBench } from '../src/public-api';

describe('NgBench - mocked directive', () => {
    let spy: jest.Mock<number>;

    @Directive({
        selector: '[smaCounter]', // notice the brackets
    })
    class CounterDirective {
        @Input('smaCounter') counter = 0;
        @Output() mouseEnterCount = new EventEmitter<number>();

        @HostListener('mouseenter') onMouseEnter() {
            this.counter++;
            this.mouseEnterCount.emit(this.counter);
        }
    }

    describe('use one time in tested component', () => {
        @Component({
            selector: 'sma-whatever',
            template: `
                <div
                    [smaCounter]="123"
                    (mouseEnterCount)="onCountChange($event)"
                >
                    Hello
                </div>
            `,
        })
        class TestedComponent {
            onCountChange(counter: number) {
                if (spy) {
                    spy(counter);
                }
            }
        }

        it('[DO] mock directive and get it through host to interact with its inputs and outputs', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockDirectives(CounterDirective)
                .createHost();

            expect(host.getInstanceByDirective(CounterDirective).counter).toBe(
                123,
            );

            spy = jest.fn();
            host.getInstanceByDirective(CounterDirective).mouseEnterCount.emit(
                456,
            );
            expect(spy).lastCalledWith(456);
        });

        it('[AVOID] using the real directive when all you want to test is the component', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .declare(CounterDirective)
                .createHost();
            spy = jest.fn();

            host.mouseEnter(host.getByText('Hello'));
            expect(spy).lastCalledWith(123 + 1);
        });
    });

    describe('use several tims in tested component', () => {
        @Component({
            selector: 'sma-whatever',
            template: `
                <div [smaCounter]="123">Hello</div>
                <div [smaCounter]="456">Hello</div>
                <div [smaCounter]="789">Hello</div>
            `,
        })
        class TestedComponent {}

        it('[DO] use host.getAllMockedDirectives', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockDirectives(CounterDirective)
                .createHost();

            const counterDirectives = host.getAllInstancesByDirective(
                CounterDirective,
            );
            expect(counterDirectives[0].counter).toBe(123);
            expect(counterDirectives[1].counter).toBe(456);
            expect(counterDirectives[2].counter).toBe(789);
        });

        it('[DO NOT] use host.getInstanceByDirective when there could be more than one mocked directive', async () => {
            const host = await NgBench.testComponent(TestedComponent)
                .mockDirectives(CounterDirective)
                .createHost();

            expect(() =>
                host.queryInstanceByDirective(CounterDirective),
            ).toThrow(
                `Found multiple directives of type "CounterDirective". If this is intentional, use the 'getAllInstancesByDirective' variant`,
            );
        });
    });
});
