import { Component, Input } from '@angular/core';
import { fakeAsync, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Host, NgBench } from '../src/public-api';

describe('NgBench - Template driven forn', () => {
    describe('basics', () => {
        describe('binding to an input element', () => {
            @Component({
                selector: 'sma-form',
                template: `
                    <input
                        placeholder="your firstname"
                        [(ngModel)]="firstName"
                        required
                    />
                `,
            })
            class TestedFormComponent {
                @Input() firstName = '';
            }

            let host: Host<TestedFormComponent>;

            beforeEach(async () => {
                host = await NgBench.testComponent(TestedFormComponent)
                    .import(FormsModule) // required for ngModel directive
                    .bindInputs({ firstName: 'Sergio' })
                    .createHost();
            });

            it('should bind firstName to input (bidirectional)', async () => {
                const input = host.getByPlaceholderText('your firstname');
                expect(input).toHaveValue('Sergio');

                host.patchInputs({ firstName: 'Isabelle' });
                await host.whenStable();
                expect(input).toHaveValue('Isabelle');

                host.clear(input);
                host.type(input, 'Philippe');
                expect(input).toHaveValue('Philippe');

                /**
                 * Template-driven forms are not synchronous, making them more complicated to test.
                 * Notice the required call to host.whenStable() for the change to be reflected
                 * in the template input.
                 */
            });

            it('[DO NOT] use host.type delay option without fake time manangement', async () => {
                const input = host.getByPlaceholderText('your firstname');
                host.clear(input);
                expect(input).toHaveValue('');

                await host.type(input, 'Isabelle', { delay: 100 });
                expect(input).toHaveValue('Isabelle');

                /**
                 * this will make your test slow as we must wait 8 * 100 ms until the input is really typed.
                 */
            });

            // NOTE: after upgrading @testing-library/angular and use @testing-library/user-event,
            // the delay cannot be simulated with fakeAsync
            // TODO: find a workaround
            it.skip('[DO] use fakeAsync, tick and host.type delay options to simulate real user typing', fakeAsync(() => {
                const input = host.getByPlaceholderText('your firstname');
                host.clear(input);

                host.type(input, 'Isabelle', { delay: 250 });

                tick(1000);
                expect(input).toHaveValue('Isab');

                tick(1000);
                expect(input).toHaveValue('Isabelle');
            }));

            describe('css classes added by angular to reflect control state', () => {
                it("has control's value changed (pristine / dirty)", async () => {
                    const input = host.getByPlaceholderText('your firstname');
                    expect(input).toHaveClass('ng-pristine');

                    host.type(input, 'Isabelle');
                    expect(input).toHaveClass('ng-dirty');

                    host.type(input, 'Sergio');
                    expect(input).toHaveClass('ng-dirty');
                    /**
                     * Notice that restoring initial value does not make control pristine again
                     */
                });

                it('is control visited (touched / untouched)', () => {
                    const input = host.getByPlaceholderText('your firstname');
                    expect(input).toHaveClass('ng-untouched');

                    host.blur(input);
                    expect(input).toHaveClass('ng-touched');
                });

                it('is input value valid (valid / invalid) ', async () => {
                    const input = host.getByPlaceholderText('your firstname');

                    host.clear(input);
                    expect(input).toHaveClass('ng-invalid');

                    host.type(input, 'Isabelle');
                    expect(input).toHaveClass('ng-valid');
                });
            });
        });

        describe('form with several inputs', () => {
            @Component({
                selector: 'sma-form',
                template: `
                    <form #loginForm="ngForm" data-testid="form">
                        <input
                            placeholder="your firstname"
                            [(ngModel)]="firstName"
                            name="firstName"
                            required
                        />

                        <select
                            data-testid="userType"
                            [(ngModel)]="userType"
                            name="userType"
                            required
                        >
                            <option
                                *ngFor="let userType of userTypes"
                                [value]="userType"
                            >
                                {{ userType }}
                            </option>
                        </select>

                        <button type="submit" [disabled]="loginForm.invalid">
                            sign in
                        </button>
                    </form>
                `,
            })
            class TestedFormComponent {
                userTypes = ['admin', 'regular', 'visitor'];

                @Input() userType = '';
                @Input() firstName = '';
            }

            let host: Host<TestedFormComponent>;

            beforeEach(async () => {
                host = await NgBench.testComponent(TestedFormComponent)
                    .bindInputs('firstName', 'userType')
                    .import(FormsModule)
                    .createHost();
            });

            it('use control name to check form values ', () => {
                const form = host.getByTestId('form');
                expect(form).toHaveFormValues({
                    firstName: '',
                    userType: undefined,
                });

                host.type(
                    host.getByPlaceholderText('your firstname'),
                    'Sergio',
                );
                host.selectOptions(host.getByTestId('userType'), 'visitor');
                expect(form).toHaveFormValues({
                    firstName: 'Sergio',
                    userType: 'visitor',
                });

                /**
                 * Here, firstName and userType keys depends on the name attribute defined within input element.
                 * It is convenient to use same name as the model, but it is not mandatory.
                 */
            });

            it("form is invalid when at least one of it's control is invalid", async () => {
                const form = host.getByTestId('form');
                const fieldFirstName = host.getByPlaceholderText(
                    'your firstname',
                );
                const fieldUserType = host.getByTestId('userType');
                const signInButton = host.getByText('sign in');

                expect(fieldFirstName).toBeInvalid();
                expect(fieldUserType).toBeInvalid();
                expect(form).toBeInvalid();
                expect(signInButton).toBeDisabled();

                host.type(fieldFirstName, 'Sergio');
                expect(fieldFirstName).toBeValid();
                expect(fieldUserType).toBeInvalid();
                expect(form).toBeInvalid();
                expect(signInButton).toBeDisabled();

                host.selectOptions(fieldUserType, 'regular');
                expect(fieldFirstName).toBeValid();
                expect(fieldUserType).toBeValid();
                expect(form).toBeValid();
                expect(signInButton).toBeEnabled();

                host.clear(fieldFirstName);
                expect(fieldFirstName).toBeInvalid();
                expect(fieldUserType).toBeValid();
                expect(form).toBeInvalid();
                expect(signInButton).toBeDisabled();
            });
        });
    });
});
