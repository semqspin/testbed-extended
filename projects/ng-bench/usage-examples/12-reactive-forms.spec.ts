import { Component } from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    ReactiveFormsModule,
    Validators,
} from '@angular/forms';
import { Host, NgBench } from '../src/public-api';

describe('NgBench - Reactive forms', () => {
    describe('basics', () => {
        describe('binding a formControl to an HTML input', () => {
            @Component({
                selector: 'sma-whatever',
                template: `
                    <label for="firstName">Name:</label>
                    <input
                        type="text"
                        id="firstName"
                        [formControl]="firstName"
                    />
                    <div>hello {{ firstName.value }}!</div>
                `,
            })
            class TestedFormComponent {
                firstName = new FormControl('', [
                    Validators.minLength(3),
                    Validators.maxLength(10),
                ]);
            }

            it('should update the formControl value when input changes', async () => {
                const host = await NgBench.testComponent(TestedFormComponent)
                    .import(ReactiveFormsModule)
                    .createHost();

                host.type(host.getByLabelText('Name:'), 'Sergio');
                expect(host.getByText('hello Sergio!')).toBeInTheDocument();
            });

            it('should update SYNCHRONOUSLY the input value when formControl value is set', async () => {
                const host = await NgBench.testComponent(TestedFormComponent)
                    .import(ReactiveFormsModule)
                    .createHost();

                host.testInstance.firstName.setValue('Isabelle');
                expect(host.getByLabelText('Name:')).toHaveValue('Isabelle');

                /**
                 * No need to host.detectChanges() for input value to be updated.
                 * However, detect changes is required when other elements displays the control value
                 */

                const div = host.container.querySelector('div');
                expect(div && div.textContent).toBe('hello !');
                host.detectChanges();
                expect(div && div.textContent).toBe('hello Isabelle!');
            });

            describe('control states', () => {
                let host: Host<TestedFormComponent>;
                const firstNameInput = () => host.getByLabelText('Name:');
                const firstNameControl = () => host.testInstance.firstName;

                beforeEach(async () => {
                    host = await NgBench.testComponent(TestedFormComponent)
                        .import(ReactiveFormsModule)
                        .createHost();
                });

                it("has control's value changed", async () => {
                    expect(firstNameControl().pristine).toBeTrue();
                    expect(firstNameControl().dirty).toBeFalse();

                    host.type(firstNameInput(), 'Isabelle');

                    expect(firstNameControl().pristine).toBeFalse();
                    expect(firstNameControl().dirty).toBeTrue();

                    firstNameControl().reset();

                    expect(firstNameControl().pristine).toBeTrue();
                    expect(firstNameControl().dirty).toBeFalse();
                });

                it('has control been visited', () => {
                    expect(firstNameControl().touched).toBeFalse();
                    expect(firstNameControl().untouched).toBeTrue();

                    host.blur(firstNameInput());

                    expect(firstNameControl().touched).toBeTrue();
                    expect(firstNameControl().untouched).toBeFalse();
                });

                it("is control's value valid", () => {
                    host.type(firstNameInput(), 'aa');

                    expect(firstNameControl().valid).toBeFalse();
                    expect(firstNameControl().invalid).toBeTrue();
                    expect(firstNameControl().hasError('minlength'));
                    expect(firstNameControl().getError('minlength')).toEqual({
                        actualLength: 2,
                        requiredLength: 3,
                    });

                    host.type(firstNameInput(), 'Sergio');

                    expect(firstNameControl().valid).toBeTrue();
                    expect(firstNameControl().invalid).toBeFalse();

                    host.clear(firstNameInput());
                    host.type(firstNameInput(), 'this is way too long');

                    expect(firstNameControl().valid).toBeFalse();
                    expect(firstNameControl().invalid).toBeTrue();

                    expect(firstNameControl().hasError('maxlength'));
                    expect(firstNameControl().getError('maxlength')).toEqual({
                        actualLength: 20,
                        requiredLength: 10,
                    });
                });
            });
        });

        describe('form groups (using FormBuilder)', () => {
            @Component({
                selector: 'sma-whatever',
                template: `
                    <form [formGroup]="userForm" role="form">
                        <input
                            type="text"
                            formControlName="firstName"
                            placeholder="firstName"
                        />
                        <input
                            type="text"
                            formControlName="lastName"
                            placeholder="lastName"
                        />
                        <select formControlName="role" role="listbox">
                            <option value="admin">Administrator</option>
                            <option value="standard">Standard user</option>
                        </select>

                        <button [disabled]="userForm.invalid">save</button>
                    </form>
                `,
            })
            class TestedFormComponent {
                userForm: FormGroup;

                constructor(fb: FormBuilder) {
                    this.userForm = fb.group({
                        firstName: ['Sergio', Validators.minLength(3)],
                        lastName: 'Mazzoleni',
                        role: 'admin',
                    });
                }
            }

            let host: Host<TestedFormComponent>;

            beforeEach(async () => {
                host = await NgBench.testComponent(TestedFormComponent)
                    .import(ReactiveFormsModule)
                    .createHost();
            });

            const inputFirstName = () => host.getByPlaceholderText('firstName');
            const inputLastName = () => host.getByPlaceholderText('lastName');
            const selectRole = () =>
                host.getByRole('listbox', { hidden: true });
            const form = () => host.getByRole('form', { hidden: true });

            it('form inputs should have values from userForm', async () => {
                expect(inputFirstName()).toHaveValue('Sergio');
                expect(inputLastName()).toHaveValue('Mazzoleni');
                expect(selectRole()).toHaveValue('admin');
            });

            it('[DO NOT] toHaveFormValues will not work with reactive forms', () => {
                expect(() => {
                    expect(form()).toHaveFormValues({
                        firstName: 'Sergio',
                        lastName: 'Mazzoleni',
                        role: 'admin',
                    });
                }).toThrow('Expected the element to have form values');
            });

            it('should update form group value when input changes', async () => {
                host.selectOptions(
                    selectRole(),
                    host.getByText('Standard user'),
                );

                expect(host.testInstance.userForm.value).toEqual({
                    firstName: 'Sergio',
                    lastName: 'Mazzoleni',
                    role: 'standard',
                });
            });

            it('use patchValue when partially updating the current value', () => {
                host.testInstance.userForm.patchValue({
                    firstName: 'Eugenio',
                });

                expect(inputFirstName()).toHaveValue('Eugenio');
                expect(inputLastName()).toHaveValue('Mazzoleni');
                expect(selectRole()).toHaveValue('admin');
            });

            it('use setValue when updating the entire current value', () => {
                host.testInstance.userForm.setValue({
                    firstName: 'Angelo',
                    lastName: 'Milingi',
                    role: 'standard',
                });

                expect(inputFirstName()).toHaveValue('Angelo');
                expect(inputLastName()).toHaveValue('Milingi');
                expect(selectRole()).toHaveValue('standard');
            });
        });
    });
});
