import { RouterTestingModule } from '@angular/router/testing';
import { Host, NgBench } from '@smazzoleni/ng-bench';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
    let host: Host<AppComponent>;

    beforeEach(async () => {
        host = await NgBench.testComponent(AppComponent)
            .import(RouterTestingModule)
            .createHost();
    });

    /// OLD

    it('should create the app', () => {
        expect(host.testInstance).toBeTruthy();
    });

    it(`should have as title 'ng-bench'`, () => {
        expect(host.testInstance.title).toEqual('ng-bench');
    });

    it('should render title', () => {
        const contentSpan = host.container.querySelector('.content span');
        expect(contentSpan && contentSpan.textContent).toContain(
            'ng-bench app is running!',
        );
    });

    // NEW

    it('should create', () => {
        expect(host.container).toBeInTheDocument();
    });

    it(`should have as title 'ng-bench'`, () => {
        expect(
            host.queryByText('ng-bench app is running!'),
        ).toBeInTheDocument();
    });
});
